package com.test.ui.opensettings;

import android.widget.Toast;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.net.Uri;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

public class OpenSettingsModule extends ReactContextBaseJavaModule{

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";


    @Override
    public String getName(){
        return "OpenSettings";
    }

    @Override
    public Map<String, Object> getConstants(){
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants; 
    }

    @ReactMethod
    public void openSettings(String settings, Callback settigsCallback){
        Activity currentActivity = getCurrentActivity();

        if(currentActivity == null){
            settigsCallback.invoke("not present activity");
            return;
        }

        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            Uri uri = Uri.fromParts("package", currentActivity.getPackageName(), null);
            intent.setData(uri);
            currentActivity.startActivity(intent);
            settigsCallback.invoke(true);
        } catch (Exception e) {
            settigsCallback.invoke(e.getMessage());
        }
    }

    public OpenSettingsModule(ReactApplicationContext context){
        super(context);
    }
}