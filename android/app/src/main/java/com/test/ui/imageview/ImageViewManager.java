package com.test.ui.imageview;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;

public class ImageViewManager extends SimpleViewManager<ImageView> implements LifecycleEventListener {

    public static final String REACT_CLASS = "ImageView";
    public static final int FIRST_COMMAND = 1;
    public static final int SECOND_COMMAND = 2;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public ImageView createViewInstance(ThemedReactContext context) {
        return new ImageView(context, null);
    }

    @ReactProp(name = "text")
    public void setText(ImageView view, String message) {
        // view.changeText(message);
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {

    }

    public Map getExportedCustomBubblingEventTypeConstants() {
        return MapBuilder.builder()
                .put("press", MapBuilder.of("phasedRegistrationNames", MapBuilder.of("bubbled", "onChange"))).build();
    }

    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of("secondCommand", SECOND_COMMAND, "firstCommand", FIRST_COMMAND);
    }

    public void receiveCommand(final ImageView view, int command, ReadableArray args) {
        switch (command) {
        case FIRST_COMMAND: {
            view.changeText("First command call");
            break;
        }
        case SECOND_COMMAND: {
            view.changeText("Second command call");
            break;
        }
        default: {
            view.changeText("Happen something wrong");
            break;
        }
        }
    }
}