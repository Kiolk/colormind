package com.test.ui.imageview;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.Map;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Button;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.test.R;

public class ImageView extends LinearLayout{

    private final ReactContext reactContext;
    private TextView text;
    private Button button;

    public ImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        reactContext = (ReactContext) context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.text_layout, this, true);
        text = (TextView) findViewById(R.id.text_view);
        button = (Button) findViewById(R.id.say_button);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MyLogs", "Test");
                sendNativeEvent("Hy from button");
                sendDeviceEvent("From other way :");
                text.setText("re");
            }
        });

        text.setText("Test text");
    }

    private void sendNativeEvent(String message){
        WritableMap event = Arguments.createMap();
        event.putString("data", message);
        ReactContext reactBaseContext = (ReactContext)getContext();
        reactBaseContext.getJSModule(RCTEventEmitter.class).receiveEvent(
            getId(),
            "press",
            event
        );
    }

    private void sendDeviceEvent(String message){
        WritableMap event = Arguments.createMap();
        event.putString("data", message);
        ReactContext reactBaseContext = (ReactContext)getContext();
        reactBaseContext
        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
        .emit("pressOn", event);
    }

    public void changeText(String message){
        text.setText(message);
    }
}