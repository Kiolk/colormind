package com.test.ui.imageview;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImageViewPackager implements ReactPackage{

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext){
        // List<ViewManager> modules = new ArrayList<>();
        // modules.add(new ImageViewManager());
        // return modules;
        return Arrays.<ViewManager>asList(
            new ImageViewManager()
        );
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext){
        return Collections.emptyList();
    }
}
