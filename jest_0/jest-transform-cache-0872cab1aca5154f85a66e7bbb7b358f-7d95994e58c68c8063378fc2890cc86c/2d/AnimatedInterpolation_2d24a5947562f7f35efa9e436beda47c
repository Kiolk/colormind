02f2a4b7346b3fe575b4a662a03870ae
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var AnimatedNode = require('./AnimatedNode');

var AnimatedWithChildren = require('./AnimatedWithChildren');

var NativeAnimatedHelper = require('../NativeAnimatedHelper');

var invariant = require('fbjs/lib/invariant');

var normalizeColor = require('normalizeColor');

var linear = function linear(t) {
  return t;
};

function createInterpolation(config) {
  if (config.outputRange && typeof config.outputRange[0] === 'string') {
    return createInterpolationFromStringOutputRange(config);
  }

  var outputRange = config.outputRange;
  checkInfiniteRange('outputRange', outputRange);
  var inputRange = config.inputRange;
  checkInfiniteRange('inputRange', inputRange);
  checkValidInputRange(inputRange);
  invariant(inputRange.length === outputRange.length, 'inputRange (' + inputRange.length + ') and outputRange (' + outputRange.length + ') must have the same length');
  var easing = config.easing || linear;
  var extrapolateLeft = 'extend';

  if (config.extrapolateLeft !== undefined) {
    extrapolateLeft = config.extrapolateLeft;
  } else if (config.extrapolate !== undefined) {
    extrapolateLeft = config.extrapolate;
  }

  var extrapolateRight = 'extend';

  if (config.extrapolateRight !== undefined) {
    extrapolateRight = config.extrapolateRight;
  } else if (config.extrapolate !== undefined) {
    extrapolateRight = config.extrapolate;
  }

  return function (input) {
    invariant(typeof input === 'number', 'Cannot interpolation an input which is not a number');
    var range = findRange(input, inputRange);
    return interpolate(input, inputRange[range], inputRange[range + 1], outputRange[range], outputRange[range + 1], easing, extrapolateLeft, extrapolateRight);
  };
}

function interpolate(input, inputMin, inputMax, outputMin, outputMax, easing, extrapolateLeft, extrapolateRight) {
  var result = input;

  if (result < inputMin) {
    if (extrapolateLeft === 'identity') {
      return result;
    } else if (extrapolateLeft === 'clamp') {
      result = inputMin;
    } else if (extrapolateLeft === 'extend') {}
  }

  if (result > inputMax) {
    if (extrapolateRight === 'identity') {
      return result;
    } else if (extrapolateRight === 'clamp') {
      result = inputMax;
    } else if (extrapolateRight === 'extend') {}
  }

  if (outputMin === outputMax) {
    return outputMin;
  }

  if (inputMin === inputMax) {
    if (input <= inputMin) {
      return outputMin;
    }

    return outputMax;
  }

  if (inputMin === -Infinity) {
    result = -result;
  } else if (inputMax === Infinity) {
    result = result - inputMin;
  } else {
    result = (result - inputMin) / (inputMax - inputMin);
  }

  result = easing(result);

  if (outputMin === -Infinity) {
    result = -result;
  } else if (outputMax === Infinity) {
    result = result + outputMin;
  } else {
    result = result * (outputMax - outputMin) + outputMin;
  }

  return result;
}

function colorToRgba(input) {
  var int32Color = normalizeColor(input);

  if (int32Color === null) {
    return input;
  }

  int32Color = int32Color || 0;
  var r = (int32Color & 0xff000000) >>> 24;
  var g = (int32Color & 0x00ff0000) >>> 16;
  var b = (int32Color & 0x0000ff00) >>> 8;
  var a = (int32Color & 0x000000ff) / 255;
  return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
}

var stringShapeRegex = /[0-9\.-]+/g;

function createInterpolationFromStringOutputRange(config) {
  var outputRange = config.outputRange;
  invariant(outputRange.length >= 2, 'Bad output range');
  outputRange = outputRange.map(colorToRgba);
  checkPattern(outputRange);
  var outputRanges = outputRange[0].match(stringShapeRegex).map(function () {
    return [];
  });
  outputRange.forEach(function (value) {
    value.match(stringShapeRegex).forEach(function (number, i) {
      outputRanges[i].push(+number);
    });
  });
  var interpolations = outputRange[0].match(stringShapeRegex).map(function (value, i) {
    return createInterpolation((0, _objectSpread2.default)({}, config, {
      outputRange: outputRanges[i]
    }));
  });
  var shouldRound = isRgbOrRgba(outputRange[0]);
  return function (input) {
    var i = 0;
    return outputRange[0].replace(stringShapeRegex, function () {
      var val = +interpolations[i++](input);
      var rounded = shouldRound && i < 4 ? Math.round(val) : Math.round(val * 1000) / 1000;
      return String(rounded);
    });
  };
}

function isRgbOrRgba(range) {
  return typeof range === 'string' && range.startsWith('rgb');
}

function checkPattern(arr) {
  var pattern = arr[0].replace(stringShapeRegex, '');

  for (var i = 1; i < arr.length; ++i) {
    invariant(pattern === arr[i].replace(stringShapeRegex, ''), 'invalid pattern ' + arr[0] + ' and ' + arr[i]);
  }
}

function findRange(input, inputRange) {
  var i;

  for (i = 1; i < inputRange.length - 1; ++i) {
    if (inputRange[i] >= input) {
      break;
    }
  }

  return i - 1;
}

function checkValidInputRange(arr) {
  invariant(arr.length >= 2, 'inputRange must have at least 2 elements');

  for (var i = 1; i < arr.length; ++i) {
    invariant(arr[i] >= arr[i - 1], 'inputRange must be monotonically non-decreasing ' + arr);
  }
}

function checkInfiniteRange(name, arr) {
  invariant(arr.length >= 2, name + ' must have at least 2 elements');
  invariant(arr.length !== 2 || arr[0] !== -Infinity || arr[1] !== Infinity, name + 'cannot be ]-infinity;+infinity[ ' + arr);
}

var AnimatedInterpolation = function (_AnimatedWithChildren) {
  (0, _inherits2.default)(AnimatedInterpolation, _AnimatedWithChildren);

  function AnimatedInterpolation(parent, config) {
    var _this;

    (0, _classCallCheck2.default)(this, AnimatedInterpolation);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(AnimatedInterpolation).call(this));
    _this._parent = parent;
    _this._config = config;
    _this._interpolation = createInterpolation(config);
    return _this;
  }

  (0, _createClass2.default)(AnimatedInterpolation, [{
    key: "__makeNative",
    value: function __makeNative() {
      this._parent.__makeNative();

      (0, _get2.default)((0, _getPrototypeOf2.default)(AnimatedInterpolation.prototype), "__makeNative", this).call(this);
    }
  }, {
    key: "__getValue",
    value: function __getValue() {
      var parentValue = this._parent.__getValue();

      invariant(typeof parentValue === 'number', 'Cannot interpolate an input which is not a number.');
      return this._interpolation(parentValue);
    }
  }, {
    key: "interpolate",
    value: function interpolate(config) {
      return new AnimatedInterpolation(this, config);
    }
  }, {
    key: "__attach",
    value: function __attach() {
      this._parent.__addChild(this);
    }
  }, {
    key: "__detach",
    value: function __detach() {
      this._parent.__removeChild(this);

      (0, _get2.default)((0, _getPrototypeOf2.default)(AnimatedInterpolation.prototype), "__detach", this).call(this);
    }
  }, {
    key: "__transformDataType",
    value: function __transformDataType(range) {
      return range.map(function (value) {
        if (typeof value !== 'string') {
          return value;
        }

        if (/deg$/.test(value)) {
          var degrees = parseFloat(value) || 0;
          var radians = degrees * Math.PI / 180.0;
          return radians;
        } else {
          return parseFloat(value) || 0;
        }
      });
    }
  }, {
    key: "__getNativeConfig",
    value: function __getNativeConfig() {
      if (__DEV__) {
        NativeAnimatedHelper.validateInterpolation(this._config);
      }

      return {
        inputRange: this._config.inputRange,
        outputRange: this.__transformDataType(this._config.outputRange),
        extrapolateLeft: this._config.extrapolateLeft || this._config.extrapolate || 'extend',
        extrapolateRight: this._config.extrapolateRight || this._config.extrapolate || 'extend',
        type: 'interpolation'
      };
    }
  }]);
  return AnimatedInterpolation;
}(AnimatedWithChildren);

AnimatedInterpolation.__createInterpolation = createInterpolation;
module.exports = AnimatedInterpolation;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFuaW1hdGVkSW50ZXJwb2xhdGlvbi5qcyJdLCJuYW1lcyI6WyJBbmltYXRlZE5vZGUiLCJyZXF1aXJlIiwiQW5pbWF0ZWRXaXRoQ2hpbGRyZW4iLCJOYXRpdmVBbmltYXRlZEhlbHBlciIsImludmFyaWFudCIsIm5vcm1hbGl6ZUNvbG9yIiwibGluZWFyIiwidCIsImNyZWF0ZUludGVycG9sYXRpb24iLCJjb25maWciLCJvdXRwdXRSYW5nZSIsImNyZWF0ZUludGVycG9sYXRpb25Gcm9tU3RyaW5nT3V0cHV0UmFuZ2UiLCJjaGVja0luZmluaXRlUmFuZ2UiLCJpbnB1dFJhbmdlIiwiY2hlY2tWYWxpZElucHV0UmFuZ2UiLCJsZW5ndGgiLCJlYXNpbmciLCJleHRyYXBvbGF0ZUxlZnQiLCJ1bmRlZmluZWQiLCJleHRyYXBvbGF0ZSIsImV4dHJhcG9sYXRlUmlnaHQiLCJpbnB1dCIsInJhbmdlIiwiZmluZFJhbmdlIiwiaW50ZXJwb2xhdGUiLCJpbnB1dE1pbiIsImlucHV0TWF4Iiwib3V0cHV0TWluIiwib3V0cHV0TWF4IiwicmVzdWx0IiwiSW5maW5pdHkiLCJjb2xvclRvUmdiYSIsImludDMyQ29sb3IiLCJyIiwiZyIsImIiLCJhIiwic3RyaW5nU2hhcGVSZWdleCIsIm1hcCIsImNoZWNrUGF0dGVybiIsIm91dHB1dFJhbmdlcyIsIm1hdGNoIiwiZm9yRWFjaCIsInZhbHVlIiwibnVtYmVyIiwiaSIsInB1c2giLCJpbnRlcnBvbGF0aW9ucyIsInNob3VsZFJvdW5kIiwiaXNSZ2JPclJnYmEiLCJyZXBsYWNlIiwidmFsIiwicm91bmRlZCIsIk1hdGgiLCJyb3VuZCIsIlN0cmluZyIsInN0YXJ0c1dpdGgiLCJhcnIiLCJwYXR0ZXJuIiwibmFtZSIsIkFuaW1hdGVkSW50ZXJwb2xhdGlvbiIsInBhcmVudCIsIl9wYXJlbnQiLCJfY29uZmlnIiwiX2ludGVycG9sYXRpb24iLCJfX21ha2VOYXRpdmUiLCJwYXJlbnRWYWx1ZSIsIl9fZ2V0VmFsdWUiLCJfX2FkZENoaWxkIiwiX19yZW1vdmVDaGlsZCIsInRlc3QiLCJkZWdyZWVzIiwicGFyc2VGbG9hdCIsInJhZGlhbnMiLCJQSSIsIl9fREVWX18iLCJ2YWxpZGF0ZUludGVycG9sYXRpb24iLCJfX3RyYW5zZm9ybURhdGFUeXBlIiwidHlwZSIsIl9fY3JlYXRlSW50ZXJwb2xhdGlvbiIsIm1vZHVsZSIsImV4cG9ydHMiXSwibWFwcGluZ3MiOiJBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxZQUFZLEdBQUdDLE9BQU8sQ0FBQyxnQkFBRCxDQUE1Qjs7QUFDQSxJQUFNQyxvQkFBb0IsR0FBR0QsT0FBTyxDQUFDLHdCQUFELENBQXBDOztBQUNBLElBQU1FLG9CQUFvQixHQUFHRixPQUFPLENBQUMseUJBQUQsQ0FBcEM7O0FBRUEsSUFBTUcsU0FBUyxHQUFHSCxPQUFPLENBQUMsb0JBQUQsQ0FBekI7O0FBQ0EsSUFBTUksY0FBYyxHQUFHSixPQUFPLENBQUMsZ0JBQUQsQ0FBOUI7O0FBaUJBLElBQU1LLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUFDLENBQUM7QUFBQSxTQUFJQSxDQUFKO0FBQUEsQ0FBaEI7O0FBTUEsU0FBU0MsbUJBQVQsQ0FDRUMsTUFERixFQUVzQztBQUNwQyxNQUFJQSxNQUFNLENBQUNDLFdBQVAsSUFBc0IsT0FBT0QsTUFBTSxDQUFDQyxXQUFQLENBQW1CLENBQW5CLENBQVAsS0FBaUMsUUFBM0QsRUFBcUU7QUFDbkUsV0FBT0Msd0NBQXdDLENBQUNGLE1BQUQsQ0FBL0M7QUFDRDs7QUFFRCxNQUFNQyxXQUEwQixHQUFJRCxNQUFNLENBQUNDLFdBQTNDO0FBQ0FFLEVBQUFBLGtCQUFrQixDQUFDLGFBQUQsRUFBZ0JGLFdBQWhCLENBQWxCO0FBRUEsTUFBTUcsVUFBVSxHQUFHSixNQUFNLENBQUNJLFVBQTFCO0FBQ0FELEVBQUFBLGtCQUFrQixDQUFDLFlBQUQsRUFBZUMsVUFBZixDQUFsQjtBQUNBQyxFQUFBQSxvQkFBb0IsQ0FBQ0QsVUFBRCxDQUFwQjtBQUVBVCxFQUFBQSxTQUFTLENBQ1BTLFVBQVUsQ0FBQ0UsTUFBWCxLQUFzQkwsV0FBVyxDQUFDSyxNQUQzQixFQUVQLGlCQUNFRixVQUFVLENBQUNFLE1BRGIsR0FFRSxxQkFGRixHQUdFTCxXQUFXLENBQUNLLE1BSGQsR0FJRSw2QkFOSyxDQUFUO0FBU0EsTUFBTUMsTUFBTSxHQUFHUCxNQUFNLENBQUNPLE1BQVAsSUFBaUJWLE1BQWhDO0FBRUEsTUFBSVcsZUFBZ0MsR0FBRyxRQUF2Qzs7QUFDQSxNQUFJUixNQUFNLENBQUNRLGVBQVAsS0FBMkJDLFNBQS9CLEVBQTBDO0FBQ3hDRCxJQUFBQSxlQUFlLEdBQUdSLE1BQU0sQ0FBQ1EsZUFBekI7QUFDRCxHQUZELE1BRU8sSUFBSVIsTUFBTSxDQUFDVSxXQUFQLEtBQXVCRCxTQUEzQixFQUFzQztBQUMzQ0QsSUFBQUEsZUFBZSxHQUFHUixNQUFNLENBQUNVLFdBQXpCO0FBQ0Q7O0FBRUQsTUFBSUMsZ0JBQWlDLEdBQUcsUUFBeEM7O0FBQ0EsTUFBSVgsTUFBTSxDQUFDVyxnQkFBUCxLQUE0QkYsU0FBaEMsRUFBMkM7QUFDekNFLElBQUFBLGdCQUFnQixHQUFHWCxNQUFNLENBQUNXLGdCQUExQjtBQUNELEdBRkQsTUFFTyxJQUFJWCxNQUFNLENBQUNVLFdBQVAsS0FBdUJELFNBQTNCLEVBQXNDO0FBQzNDRSxJQUFBQSxnQkFBZ0IsR0FBR1gsTUFBTSxDQUFDVSxXQUExQjtBQUNEOztBQUVELFNBQU8sVUFBQUUsS0FBSyxFQUFJO0FBQ2RqQixJQUFBQSxTQUFTLENBQ1AsT0FBT2lCLEtBQVAsS0FBaUIsUUFEVixFQUVQLHFEQUZPLENBQVQ7QUFLQSxRQUFNQyxLQUFLLEdBQUdDLFNBQVMsQ0FBQ0YsS0FBRCxFQUFRUixVQUFSLENBQXZCO0FBQ0EsV0FBT1csV0FBVyxDQUNoQkgsS0FEZ0IsRUFFaEJSLFVBQVUsQ0FBQ1MsS0FBRCxDQUZNLEVBR2hCVCxVQUFVLENBQUNTLEtBQUssR0FBRyxDQUFULENBSE0sRUFJaEJaLFdBQVcsQ0FBQ1ksS0FBRCxDQUpLLEVBS2hCWixXQUFXLENBQUNZLEtBQUssR0FBRyxDQUFULENBTEssRUFNaEJOLE1BTmdCLEVBT2hCQyxlQVBnQixFQVFoQkcsZ0JBUmdCLENBQWxCO0FBVUQsR0FqQkQ7QUFrQkQ7O0FBRUQsU0FBU0ksV0FBVCxDQUNFSCxLQURGLEVBRUVJLFFBRkYsRUFHRUMsUUFIRixFQUlFQyxTQUpGLEVBS0VDLFNBTEYsRUFNRVosTUFORixFQU9FQyxlQVBGLEVBUUVHLGdCQVJGLEVBU0U7QUFDQSxNQUFJUyxNQUFNLEdBQUdSLEtBQWI7O0FBR0EsTUFBSVEsTUFBTSxHQUFHSixRQUFiLEVBQXVCO0FBQ3JCLFFBQUlSLGVBQWUsS0FBSyxVQUF4QixFQUFvQztBQUNsQyxhQUFPWSxNQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUlaLGVBQWUsS0FBSyxPQUF4QixFQUFpQztBQUN0Q1ksTUFBQUEsTUFBTSxHQUFHSixRQUFUO0FBQ0QsS0FGTSxNQUVBLElBQUlSLGVBQWUsS0FBSyxRQUF4QixFQUFrQyxDQUV4QztBQUNGOztBQUVELE1BQUlZLE1BQU0sR0FBR0gsUUFBYixFQUF1QjtBQUNyQixRQUFJTixnQkFBZ0IsS0FBSyxVQUF6QixFQUFxQztBQUNuQyxhQUFPUyxNQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUlULGdCQUFnQixLQUFLLE9BQXpCLEVBQWtDO0FBQ3ZDUyxNQUFBQSxNQUFNLEdBQUdILFFBQVQ7QUFDRCxLQUZNLE1BRUEsSUFBSU4sZ0JBQWdCLEtBQUssUUFBekIsRUFBbUMsQ0FFekM7QUFDRjs7QUFFRCxNQUFJTyxTQUFTLEtBQUtDLFNBQWxCLEVBQTZCO0FBQzNCLFdBQU9ELFNBQVA7QUFDRDs7QUFFRCxNQUFJRixRQUFRLEtBQUtDLFFBQWpCLEVBQTJCO0FBQ3pCLFFBQUlMLEtBQUssSUFBSUksUUFBYixFQUF1QjtBQUNyQixhQUFPRSxTQUFQO0FBQ0Q7O0FBQ0QsV0FBT0MsU0FBUDtBQUNEOztBQUdELE1BQUlILFFBQVEsS0FBSyxDQUFDSyxRQUFsQixFQUE0QjtBQUMxQkQsSUFBQUEsTUFBTSxHQUFHLENBQUNBLE1BQVY7QUFDRCxHQUZELE1BRU8sSUFBSUgsUUFBUSxLQUFLSSxRQUFqQixFQUEyQjtBQUNoQ0QsSUFBQUEsTUFBTSxHQUFHQSxNQUFNLEdBQUdKLFFBQWxCO0FBQ0QsR0FGTSxNQUVBO0FBQ0xJLElBQUFBLE1BQU0sR0FBRyxDQUFDQSxNQUFNLEdBQUdKLFFBQVYsS0FBdUJDLFFBQVEsR0FBR0QsUUFBbEMsQ0FBVDtBQUNEOztBQUdESSxFQUFBQSxNQUFNLEdBQUdiLE1BQU0sQ0FBQ2EsTUFBRCxDQUFmOztBQUdBLE1BQUlGLFNBQVMsS0FBSyxDQUFDRyxRQUFuQixFQUE2QjtBQUMzQkQsSUFBQUEsTUFBTSxHQUFHLENBQUNBLE1BQVY7QUFDRCxHQUZELE1BRU8sSUFBSUQsU0FBUyxLQUFLRSxRQUFsQixFQUE0QjtBQUNqQ0QsSUFBQUEsTUFBTSxHQUFHQSxNQUFNLEdBQUdGLFNBQWxCO0FBQ0QsR0FGTSxNQUVBO0FBQ0xFLElBQUFBLE1BQU0sR0FBR0EsTUFBTSxJQUFJRCxTQUFTLEdBQUdELFNBQWhCLENBQU4sR0FBbUNBLFNBQTVDO0FBQ0Q7O0FBRUQsU0FBT0UsTUFBUDtBQUNEOztBQUVELFNBQVNFLFdBQVQsQ0FBcUJWLEtBQXJCLEVBQTRDO0FBQzFDLE1BQUlXLFVBQVUsR0FBRzNCLGNBQWMsQ0FBQ2dCLEtBQUQsQ0FBL0I7O0FBQ0EsTUFBSVcsVUFBVSxLQUFLLElBQW5CLEVBQXlCO0FBQ3ZCLFdBQU9YLEtBQVA7QUFDRDs7QUFFRFcsRUFBQUEsVUFBVSxHQUFHQSxVQUFVLElBQUksQ0FBM0I7QUFFQSxNQUFNQyxDQUFDLEdBQUcsQ0FBQ0QsVUFBVSxHQUFHLFVBQWQsTUFBOEIsRUFBeEM7QUFDQSxNQUFNRSxDQUFDLEdBQUcsQ0FBQ0YsVUFBVSxHQUFHLFVBQWQsTUFBOEIsRUFBeEM7QUFDQSxNQUFNRyxDQUFDLEdBQUcsQ0FBQ0gsVUFBVSxHQUFHLFVBQWQsTUFBOEIsQ0FBeEM7QUFDQSxNQUFNSSxDQUFDLEdBQUcsQ0FBQ0osVUFBVSxHQUFHLFVBQWQsSUFBNEIsR0FBdEM7QUFFQSxtQkFBZUMsQ0FBZixVQUFxQkMsQ0FBckIsVUFBMkJDLENBQTNCLFVBQWlDQyxDQUFqQztBQUNEOztBQUVELElBQU1DLGdCQUFnQixHQUFHLFlBQXpCOztBQVVBLFNBQVMxQix3Q0FBVCxDQUNFRixNQURGLEVBRTZCO0FBQzNCLE1BQUlDLFdBQTBCLEdBQUlELE1BQU0sQ0FBQ0MsV0FBekM7QUFDQU4sRUFBQUEsU0FBUyxDQUFDTSxXQUFXLENBQUNLLE1BQVosSUFBc0IsQ0FBdkIsRUFBMEIsa0JBQTFCLENBQVQ7QUFDQUwsRUFBQUEsV0FBVyxHQUFHQSxXQUFXLENBQUM0QixHQUFaLENBQWdCUCxXQUFoQixDQUFkO0FBQ0FRLEVBQUFBLFlBQVksQ0FBQzdCLFdBQUQsQ0FBWjtBQWFBLE1BQU04QixZQUFZLEdBQUc5QixXQUFXLENBQUMsQ0FBRCxDQUFYLENBQWUrQixLQUFmLENBQXFCSixnQkFBckIsRUFBdUNDLEdBQXZDLENBQTJDO0FBQUEsV0FBTSxFQUFOO0FBQUEsR0FBM0MsQ0FBckI7QUFDQTVCLEVBQUFBLFdBQVcsQ0FBQ2dDLE9BQVosQ0FBb0IsVUFBQUMsS0FBSyxFQUFJO0FBSTNCQSxJQUFBQSxLQUFLLENBQUNGLEtBQU4sQ0FBWUosZ0JBQVosRUFBOEJLLE9BQTlCLENBQXNDLFVBQUNFLE1BQUQsRUFBU0MsQ0FBVCxFQUFlO0FBQ25ETCxNQUFBQSxZQUFZLENBQUNLLENBQUQsQ0FBWixDQUFnQkMsSUFBaEIsQ0FBcUIsQ0FBQ0YsTUFBdEI7QUFDRCxLQUZEO0FBR0QsR0FQRDtBQVlBLE1BQU1HLGNBQWMsR0FBR3JDLFdBQVcsQ0FBQyxDQUFELENBQVgsQ0FDcEIrQixLQURvQixDQUNkSixnQkFEYyxFQUVwQkMsR0FGb0IsQ0FFaEIsVUFBQ0ssS0FBRCxFQUFRRSxDQUFSLEVBQWM7QUFDakIsV0FBT3JDLG1CQUFtQixpQ0FDckJDLE1BRHFCO0FBRXhCQyxNQUFBQSxXQUFXLEVBQUU4QixZQUFZLENBQUNLLENBQUQ7QUFGRCxPQUExQjtBQUlELEdBUG9CLENBQXZCO0FBV0EsTUFBTUcsV0FBVyxHQUFHQyxXQUFXLENBQUN2QyxXQUFXLENBQUMsQ0FBRCxDQUFaLENBQS9CO0FBRUEsU0FBTyxVQUFBVyxLQUFLLEVBQUk7QUFDZCxRQUFJd0IsQ0FBQyxHQUFHLENBQVI7QUFJQSxXQUFPbkMsV0FBVyxDQUFDLENBQUQsQ0FBWCxDQUFld0MsT0FBZixDQUF1QmIsZ0JBQXZCLEVBQXlDLFlBQU07QUFDcEQsVUFBTWMsR0FBRyxHQUFHLENBQUNKLGNBQWMsQ0FBQ0YsQ0FBQyxFQUFGLENBQWQsQ0FBb0J4QixLQUFwQixDQUFiO0FBQ0EsVUFBTStCLE9BQU8sR0FDWEosV0FBVyxJQUFJSCxDQUFDLEdBQUcsQ0FBbkIsR0FBdUJRLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxHQUFYLENBQXZCLEdBQXlDRSxJQUFJLENBQUNDLEtBQUwsQ0FBV0gsR0FBRyxHQUFHLElBQWpCLElBQXlCLElBRHBFO0FBRUEsYUFBT0ksTUFBTSxDQUFDSCxPQUFELENBQWI7QUFDRCxLQUxNLENBQVA7QUFNRCxHQVhEO0FBWUQ7O0FBRUQsU0FBU0gsV0FBVCxDQUFxQjNCLEtBQXJCLEVBQTRCO0FBQzFCLFNBQU8sT0FBT0EsS0FBUCxLQUFpQixRQUFqQixJQUE2QkEsS0FBSyxDQUFDa0MsVUFBTixDQUFpQixLQUFqQixDQUFwQztBQUNEOztBQUVELFNBQVNqQixZQUFULENBQXNCa0IsR0FBdEIsRUFBMEM7QUFDeEMsTUFBTUMsT0FBTyxHQUFHRCxHQUFHLENBQUMsQ0FBRCxDQUFILENBQU9QLE9BQVAsQ0FBZWIsZ0JBQWYsRUFBaUMsRUFBakMsQ0FBaEI7O0FBQ0EsT0FBSyxJQUFJUSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWSxHQUFHLENBQUMxQyxNQUF4QixFQUFnQyxFQUFFOEIsQ0FBbEMsRUFBcUM7QUFDbkN6QyxJQUFBQSxTQUFTLENBQ1BzRCxPQUFPLEtBQUtELEdBQUcsQ0FBQ1osQ0FBRCxDQUFILENBQU9LLE9BQVAsQ0FBZWIsZ0JBQWYsRUFBaUMsRUFBakMsQ0FETCxFQUVQLHFCQUFxQm9CLEdBQUcsQ0FBQyxDQUFELENBQXhCLEdBQThCLE9BQTlCLEdBQXdDQSxHQUFHLENBQUNaLENBQUQsQ0FGcEMsQ0FBVDtBQUlEO0FBQ0Y7O0FBRUQsU0FBU3RCLFNBQVQsQ0FBbUJGLEtBQW5CLEVBQWtDUixVQUFsQyxFQUE2RDtBQUMzRCxNQUFJZ0MsQ0FBSjs7QUFDQSxPQUFLQSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdoQyxVQUFVLENBQUNFLE1BQVgsR0FBb0IsQ0FBcEMsRUFBdUMsRUFBRThCLENBQXpDLEVBQTRDO0FBQzFDLFFBQUloQyxVQUFVLENBQUNnQyxDQUFELENBQVYsSUFBaUJ4QixLQUFyQixFQUE0QjtBQUMxQjtBQUNEO0FBQ0Y7O0FBQ0QsU0FBT3dCLENBQUMsR0FBRyxDQUFYO0FBQ0Q7O0FBRUQsU0FBUy9CLG9CQUFULENBQThCMkMsR0FBOUIsRUFBa0Q7QUFDaERyRCxFQUFBQSxTQUFTLENBQUNxRCxHQUFHLENBQUMxQyxNQUFKLElBQWMsQ0FBZixFQUFrQiwwQ0FBbEIsQ0FBVDs7QUFDQSxPQUFLLElBQUk4QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWSxHQUFHLENBQUMxQyxNQUF4QixFQUFnQyxFQUFFOEIsQ0FBbEMsRUFBcUM7QUFDbkN6QyxJQUFBQSxTQUFTLENBQ1BxRCxHQUFHLENBQUNaLENBQUQsQ0FBSCxJQUFVWSxHQUFHLENBQUNaLENBQUMsR0FBRyxDQUFMLENBRE4sRUFRUCxxREFBcURZLEdBUjlDLENBQVQ7QUFVRDtBQUNGOztBQUVELFNBQVM3QyxrQkFBVCxDQUE0QitDLElBQTVCLEVBQTBDRixHQUExQyxFQUE4RDtBQUM1RHJELEVBQUFBLFNBQVMsQ0FBQ3FELEdBQUcsQ0FBQzFDLE1BQUosSUFBYyxDQUFmLEVBQWtCNEMsSUFBSSxHQUFHLGdDQUF6QixDQUFUO0FBQ0F2RCxFQUFBQSxTQUFTLENBQ1BxRCxHQUFHLENBQUMxQyxNQUFKLEtBQWUsQ0FBZixJQUFvQjBDLEdBQUcsQ0FBQyxDQUFELENBQUgsS0FBVyxDQUFDM0IsUUFBaEMsSUFBNEMyQixHQUFHLENBQUMsQ0FBRCxDQUFILEtBQVczQixRQURoRCxFQVFQNkIsSUFBSSxHQUFHLGtDQUFQLEdBQTRDRixHQVJyQyxDQUFUO0FBVUQ7O0lBRUtHLHFCOzs7QUFRSixpQ0FBWUMsTUFBWixFQUFrQ3BELE1BQWxDLEVBQW1FO0FBQUE7O0FBQUE7QUFDakU7QUFDQSxVQUFLcUQsT0FBTCxHQUFlRCxNQUFmO0FBQ0EsVUFBS0UsT0FBTCxHQUFldEQsTUFBZjtBQUNBLFVBQUt1RCxjQUFMLEdBQXNCeEQsbUJBQW1CLENBQUNDLE1BQUQsQ0FBekM7QUFKaUU7QUFLbEU7Ozs7bUNBRWM7QUFDYixXQUFLcUQsT0FBTCxDQUFhRyxZQUFiOztBQUNBO0FBQ0Q7OztpQ0FFNkI7QUFDNUIsVUFBTUMsV0FBbUIsR0FBRyxLQUFLSixPQUFMLENBQWFLLFVBQWIsRUFBNUI7O0FBQ0EvRCxNQUFBQSxTQUFTLENBQ1AsT0FBTzhELFdBQVAsS0FBdUIsUUFEaEIsRUFFUCxvREFGTyxDQUFUO0FBSUEsYUFBTyxLQUFLRixjQUFMLENBQW9CRSxXQUFwQixDQUFQO0FBQ0Q7OztnQ0FFV3pELE0sRUFBd0Q7QUFDbEUsYUFBTyxJQUFJbUQscUJBQUosQ0FBMEIsSUFBMUIsRUFBZ0NuRCxNQUFoQyxDQUFQO0FBQ0Q7OzsrQkFFZ0I7QUFDZixXQUFLcUQsT0FBTCxDQUFhTSxVQUFiLENBQXdCLElBQXhCO0FBQ0Q7OzsrQkFFZ0I7QUFDZixXQUFLTixPQUFMLENBQWFPLGFBQWIsQ0FBMkIsSUFBM0I7O0FBQ0E7QUFDRDs7O3dDQUVtQi9DLEssRUFBbUI7QUFNckMsYUFBT0EsS0FBSyxDQUFDZ0IsR0FBTixDQUFVLFVBQVNLLEtBQVQsRUFBZ0I7QUFDL0IsWUFBSSxPQUFPQSxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO0FBQzdCLGlCQUFPQSxLQUFQO0FBQ0Q7O0FBQ0QsWUFBSSxPQUFPMkIsSUFBUCxDQUFZM0IsS0FBWixDQUFKLEVBQXdCO0FBQ3RCLGNBQU00QixPQUFPLEdBQUdDLFVBQVUsQ0FBQzdCLEtBQUQsQ0FBVixJQUFxQixDQUFyQztBQUNBLGNBQU04QixPQUFPLEdBQUlGLE9BQU8sR0FBR2xCLElBQUksQ0FBQ3FCLEVBQWhCLEdBQXNCLEtBQXRDO0FBQ0EsaUJBQU9ELE9BQVA7QUFDRCxTQUpELE1BSU87QUFFTCxpQkFBT0QsVUFBVSxDQUFDN0IsS0FBRCxDQUFWLElBQXFCLENBQTVCO0FBQ0Q7QUFDRixPQVpNLENBQVA7QUFhRDs7O3dDQUV3QjtBQUN2QixVQUFJZ0MsT0FBSixFQUFhO0FBQ1h4RSxRQUFBQSxvQkFBb0IsQ0FBQ3lFLHFCQUFyQixDQUEyQyxLQUFLYixPQUFoRDtBQUNEOztBQUVELGFBQU87QUFDTGxELFFBQUFBLFVBQVUsRUFBRSxLQUFLa0QsT0FBTCxDQUFhbEQsVUFEcEI7QUFHTEgsUUFBQUEsV0FBVyxFQUFFLEtBQUttRSxtQkFBTCxDQUF5QixLQUFLZCxPQUFMLENBQWFyRCxXQUF0QyxDQUhSO0FBSUxPLFFBQUFBLGVBQWUsRUFDYixLQUFLOEMsT0FBTCxDQUFhOUMsZUFBYixJQUFnQyxLQUFLOEMsT0FBTCxDQUFhNUMsV0FBN0MsSUFBNEQsUUFMekQ7QUFNTEMsUUFBQUEsZ0JBQWdCLEVBQ2QsS0FBSzJDLE9BQUwsQ0FBYTNDLGdCQUFiLElBQWlDLEtBQUsyQyxPQUFMLENBQWE1QyxXQUE5QyxJQUE2RCxRQVAxRDtBQVFMMkQsUUFBQUEsSUFBSSxFQUFFO0FBUkQsT0FBUDtBQVVEOzs7RUE5RWlDNUUsb0I7O0FBQTlCMEQscUIsQ0FFR21CLHFCLEdBQXdCdkUsbUI7QUErRWpDd0UsTUFBTSxDQUFDQyxPQUFQLEdBQWlCckIscUJBQWpCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIEZhY2Vib29rLCBJbmMuIGFuZCBpdHMgYWZmaWxpYXRlcy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKiBAZmxvd1xuICogQGZvcm1hdFxuICovXG4vKiBlc2xpbnQgbm8tYml0d2lzZTogMCAqL1xuJ3VzZSBzdHJpY3QnO1xuXG5jb25zdCBBbmltYXRlZE5vZGUgPSByZXF1aXJlKCcuL0FuaW1hdGVkTm9kZScpO1xuY29uc3QgQW5pbWF0ZWRXaXRoQ2hpbGRyZW4gPSByZXF1aXJlKCcuL0FuaW1hdGVkV2l0aENoaWxkcmVuJyk7XG5jb25zdCBOYXRpdmVBbmltYXRlZEhlbHBlciA9IHJlcXVpcmUoJy4uL05hdGl2ZUFuaW1hdGVkSGVscGVyJyk7XG5cbmNvbnN0IGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuY29uc3Qgbm9ybWFsaXplQ29sb3IgPSByZXF1aXJlKCdub3JtYWxpemVDb2xvcicpO1xuXG50eXBlIEV4dHJhcG9sYXRlVHlwZSA9ICdleHRlbmQnIHwgJ2lkZW50aXR5JyB8ICdjbGFtcCc7XG5cbmV4cG9ydCB0eXBlIEludGVycG9sYXRpb25Db25maWdUeXBlID0ge1xuICBpbnB1dFJhbmdlOiBBcnJheTxudW1iZXI+LFxuICAvKiAkRmxvd0ZpeE1lKD49MC4zOC4wIHNpdGU9cmVhY3RfbmF0aXZlX2ZiLHJlYWN0X25hdGl2ZV9vc3MpIC0gRmxvdyBlcnJvclxuICAgKiBkZXRlY3RlZCBkdXJpbmcgdGhlIGRlcGxveW1lbnQgb2YgdjAuMzguMC4gVG8gc2VlIHRoZSBlcnJvciwgcmVtb3ZlIHRoaXNcbiAgICogY29tbWVudCBhbmQgcnVuIGZsb3dcbiAgICovXG4gIG91dHB1dFJhbmdlOiBBcnJheTxudW1iZXI+IHwgQXJyYXk8c3RyaW5nPixcbiAgZWFzaW5nPzogKGlucHV0OiBudW1iZXIpID0+IG51bWJlcixcbiAgZXh0cmFwb2xhdGU/OiBFeHRyYXBvbGF0ZVR5cGUsXG4gIGV4dHJhcG9sYXRlTGVmdD86IEV4dHJhcG9sYXRlVHlwZSxcbiAgZXh0cmFwb2xhdGVSaWdodD86IEV4dHJhcG9sYXRlVHlwZSxcbn07XG5cbmNvbnN0IGxpbmVhciA9IHQgPT4gdDtcblxuLyoqXG4gKiBWZXJ5IGhhbmR5IGhlbHBlciB0byBtYXAgaW5wdXQgcmFuZ2VzIHRvIG91dHB1dCByYW5nZXMgd2l0aCBhbiBlYXNpbmdcbiAqIGZ1bmN0aW9uIGFuZCBjdXN0b20gYmVoYXZpb3Igb3V0c2lkZSBvZiB0aGUgcmFuZ2VzLlxuICovXG5mdW5jdGlvbiBjcmVhdGVJbnRlcnBvbGF0aW9uKFxuICBjb25maWc6IEludGVycG9sYXRpb25Db25maWdUeXBlLFxuKTogKGlucHV0OiBudW1iZXIpID0+IG51bWJlciB8IHN0cmluZyB7XG4gIGlmIChjb25maWcub3V0cHV0UmFuZ2UgJiYgdHlwZW9mIGNvbmZpZy5vdXRwdXRSYW5nZVswXSA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gY3JlYXRlSW50ZXJwb2xhdGlvbkZyb21TdHJpbmdPdXRwdXRSYW5nZShjb25maWcpO1xuICB9XG5cbiAgY29uc3Qgb3V0cHV0UmFuZ2U6IEFycmF5PG51bWJlcj4gPSAoY29uZmlnLm91dHB1dFJhbmdlOiBhbnkpO1xuICBjaGVja0luZmluaXRlUmFuZ2UoJ291dHB1dFJhbmdlJywgb3V0cHV0UmFuZ2UpO1xuXG4gIGNvbnN0IGlucHV0UmFuZ2UgPSBjb25maWcuaW5wdXRSYW5nZTtcbiAgY2hlY2tJbmZpbml0ZVJhbmdlKCdpbnB1dFJhbmdlJywgaW5wdXRSYW5nZSk7XG4gIGNoZWNrVmFsaWRJbnB1dFJhbmdlKGlucHV0UmFuZ2UpO1xuXG4gIGludmFyaWFudChcbiAgICBpbnB1dFJhbmdlLmxlbmd0aCA9PT0gb3V0cHV0UmFuZ2UubGVuZ3RoLFxuICAgICdpbnB1dFJhbmdlICgnICtcbiAgICAgIGlucHV0UmFuZ2UubGVuZ3RoICtcbiAgICAgICcpIGFuZCBvdXRwdXRSYW5nZSAoJyArXG4gICAgICBvdXRwdXRSYW5nZS5sZW5ndGggK1xuICAgICAgJykgbXVzdCBoYXZlIHRoZSBzYW1lIGxlbmd0aCcsXG4gICk7XG5cbiAgY29uc3QgZWFzaW5nID0gY29uZmlnLmVhc2luZyB8fCBsaW5lYXI7XG5cbiAgbGV0IGV4dHJhcG9sYXRlTGVmdDogRXh0cmFwb2xhdGVUeXBlID0gJ2V4dGVuZCc7XG4gIGlmIChjb25maWcuZXh0cmFwb2xhdGVMZWZ0ICE9PSB1bmRlZmluZWQpIHtcbiAgICBleHRyYXBvbGF0ZUxlZnQgPSBjb25maWcuZXh0cmFwb2xhdGVMZWZ0O1xuICB9IGVsc2UgaWYgKGNvbmZpZy5leHRyYXBvbGF0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgZXh0cmFwb2xhdGVMZWZ0ID0gY29uZmlnLmV4dHJhcG9sYXRlO1xuICB9XG5cbiAgbGV0IGV4dHJhcG9sYXRlUmlnaHQ6IEV4dHJhcG9sYXRlVHlwZSA9ICdleHRlbmQnO1xuICBpZiAoY29uZmlnLmV4dHJhcG9sYXRlUmlnaHQgIT09IHVuZGVmaW5lZCkge1xuICAgIGV4dHJhcG9sYXRlUmlnaHQgPSBjb25maWcuZXh0cmFwb2xhdGVSaWdodDtcbiAgfSBlbHNlIGlmIChjb25maWcuZXh0cmFwb2xhdGUgIT09IHVuZGVmaW5lZCkge1xuICAgIGV4dHJhcG9sYXRlUmlnaHQgPSBjb25maWcuZXh0cmFwb2xhdGU7XG4gIH1cblxuICByZXR1cm4gaW5wdXQgPT4ge1xuICAgIGludmFyaWFudChcbiAgICAgIHR5cGVvZiBpbnB1dCA9PT0gJ251bWJlcicsXG4gICAgICAnQ2Fubm90IGludGVycG9sYXRpb24gYW4gaW5wdXQgd2hpY2ggaXMgbm90IGEgbnVtYmVyJyxcbiAgICApO1xuXG4gICAgY29uc3QgcmFuZ2UgPSBmaW5kUmFuZ2UoaW5wdXQsIGlucHV0UmFuZ2UpO1xuICAgIHJldHVybiBpbnRlcnBvbGF0ZShcbiAgICAgIGlucHV0LFxuICAgICAgaW5wdXRSYW5nZVtyYW5nZV0sXG4gICAgICBpbnB1dFJhbmdlW3JhbmdlICsgMV0sXG4gICAgICBvdXRwdXRSYW5nZVtyYW5nZV0sXG4gICAgICBvdXRwdXRSYW5nZVtyYW5nZSArIDFdLFxuICAgICAgZWFzaW5nLFxuICAgICAgZXh0cmFwb2xhdGVMZWZ0LFxuICAgICAgZXh0cmFwb2xhdGVSaWdodCxcbiAgICApO1xuICB9O1xufVxuXG5mdW5jdGlvbiBpbnRlcnBvbGF0ZShcbiAgaW5wdXQ6IG51bWJlcixcbiAgaW5wdXRNaW46IG51bWJlcixcbiAgaW5wdXRNYXg6IG51bWJlcixcbiAgb3V0cHV0TWluOiBudW1iZXIsXG4gIG91dHB1dE1heDogbnVtYmVyLFxuICBlYXNpbmc6IChpbnB1dDogbnVtYmVyKSA9PiBudW1iZXIsXG4gIGV4dHJhcG9sYXRlTGVmdDogRXh0cmFwb2xhdGVUeXBlLFxuICBleHRyYXBvbGF0ZVJpZ2h0OiBFeHRyYXBvbGF0ZVR5cGUsXG4pIHtcbiAgbGV0IHJlc3VsdCA9IGlucHV0O1xuXG4gIC8vIEV4dHJhcG9sYXRlXG4gIGlmIChyZXN1bHQgPCBpbnB1dE1pbikge1xuICAgIGlmIChleHRyYXBvbGF0ZUxlZnQgPT09ICdpZGVudGl0eScpIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSBlbHNlIGlmIChleHRyYXBvbGF0ZUxlZnQgPT09ICdjbGFtcCcpIHtcbiAgICAgIHJlc3VsdCA9IGlucHV0TWluO1xuICAgIH0gZWxzZSBpZiAoZXh0cmFwb2xhdGVMZWZ0ID09PSAnZXh0ZW5kJykge1xuICAgICAgLy8gbm9vcFxuICAgIH1cbiAgfVxuXG4gIGlmIChyZXN1bHQgPiBpbnB1dE1heCkge1xuICAgIGlmIChleHRyYXBvbGF0ZVJpZ2h0ID09PSAnaWRlbnRpdHknKSB7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0gZWxzZSBpZiAoZXh0cmFwb2xhdGVSaWdodCA9PT0gJ2NsYW1wJykge1xuICAgICAgcmVzdWx0ID0gaW5wdXRNYXg7XG4gICAgfSBlbHNlIGlmIChleHRyYXBvbGF0ZVJpZ2h0ID09PSAnZXh0ZW5kJykge1xuICAgICAgLy8gbm9vcFxuICAgIH1cbiAgfVxuXG4gIGlmIChvdXRwdXRNaW4gPT09IG91dHB1dE1heCkge1xuICAgIHJldHVybiBvdXRwdXRNaW47XG4gIH1cblxuICBpZiAoaW5wdXRNaW4gPT09IGlucHV0TWF4KSB7XG4gICAgaWYgKGlucHV0IDw9IGlucHV0TWluKSB7XG4gICAgICByZXR1cm4gb3V0cHV0TWluO1xuICAgIH1cbiAgICByZXR1cm4gb3V0cHV0TWF4O1xuICB9XG5cbiAgLy8gSW5wdXQgUmFuZ2VcbiAgaWYgKGlucHV0TWluID09PSAtSW5maW5pdHkpIHtcbiAgICByZXN1bHQgPSAtcmVzdWx0O1xuICB9IGVsc2UgaWYgKGlucHV0TWF4ID09PSBJbmZpbml0eSkge1xuICAgIHJlc3VsdCA9IHJlc3VsdCAtIGlucHV0TWluO1xuICB9IGVsc2Uge1xuICAgIHJlc3VsdCA9IChyZXN1bHQgLSBpbnB1dE1pbikgLyAoaW5wdXRNYXggLSBpbnB1dE1pbik7XG4gIH1cblxuICAvLyBFYXNpbmdcbiAgcmVzdWx0ID0gZWFzaW5nKHJlc3VsdCk7XG5cbiAgLy8gT3V0cHV0IFJhbmdlXG4gIGlmIChvdXRwdXRNaW4gPT09IC1JbmZpbml0eSkge1xuICAgIHJlc3VsdCA9IC1yZXN1bHQ7XG4gIH0gZWxzZSBpZiAob3V0cHV0TWF4ID09PSBJbmZpbml0eSkge1xuICAgIHJlc3VsdCA9IHJlc3VsdCArIG91dHB1dE1pbjtcbiAgfSBlbHNlIHtcbiAgICByZXN1bHQgPSByZXN1bHQgKiAob3V0cHV0TWF4IC0gb3V0cHV0TWluKSArIG91dHB1dE1pbjtcbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmZ1bmN0aW9uIGNvbG9yVG9SZ2JhKGlucHV0OiBzdHJpbmcpOiBzdHJpbmcge1xuICBsZXQgaW50MzJDb2xvciA9IG5vcm1hbGl6ZUNvbG9yKGlucHV0KTtcbiAgaWYgKGludDMyQ29sb3IgPT09IG51bGwpIHtcbiAgICByZXR1cm4gaW5wdXQ7XG4gIH1cblxuICBpbnQzMkNvbG9yID0gaW50MzJDb2xvciB8fCAwO1xuXG4gIGNvbnN0IHIgPSAoaW50MzJDb2xvciAmIDB4ZmYwMDAwMDApID4+PiAyNDtcbiAgY29uc3QgZyA9IChpbnQzMkNvbG9yICYgMHgwMGZmMDAwMCkgPj4+IDE2O1xuICBjb25zdCBiID0gKGludDMyQ29sb3IgJiAweDAwMDBmZjAwKSA+Pj4gODtcbiAgY29uc3QgYSA9IChpbnQzMkNvbG9yICYgMHgwMDAwMDBmZikgLyAyNTU7XG5cbiAgcmV0dXJuIGByZ2JhKCR7cn0sICR7Z30sICR7Yn0sICR7YX0pYDtcbn1cblxuY29uc3Qgc3RyaW5nU2hhcGVSZWdleCA9IC9bMC05XFwuLV0rL2c7XG5cbi8qKlxuICogU3VwcG9ydHMgc3RyaW5nIHNoYXBlcyBieSBleHRyYWN0aW5nIG51bWJlcnMgc28gbmV3IHZhbHVlcyBjYW4gYmUgY29tcHV0ZWQsXG4gKiBhbmQgcmVjb21iaW5lcyB0aG9zZSB2YWx1ZXMgaW50byBuZXcgc3RyaW5ncyBvZiB0aGUgc2FtZSBzaGFwZS4gIFN1cHBvcnRzXG4gKiB0aGluZ3MgbGlrZTpcbiAqXG4gKiAgIHJnYmEoMTIzLCA0MiwgOTksIDAuMzYpIC8vIGNvbG9yc1xuICogICAtNDVkZWcgICAgICAgICAgICAgICAgICAvLyB2YWx1ZXMgd2l0aCB1bml0c1xuICovXG5mdW5jdGlvbiBjcmVhdGVJbnRlcnBvbGF0aW9uRnJvbVN0cmluZ091dHB1dFJhbmdlKFxuICBjb25maWc6IEludGVycG9sYXRpb25Db25maWdUeXBlLFxuKTogKGlucHV0OiBudW1iZXIpID0+IHN0cmluZyB7XG4gIGxldCBvdXRwdXRSYW5nZTogQXJyYXk8c3RyaW5nPiA9IChjb25maWcub3V0cHV0UmFuZ2U6IGFueSk7XG4gIGludmFyaWFudChvdXRwdXRSYW5nZS5sZW5ndGggPj0gMiwgJ0JhZCBvdXRwdXQgcmFuZ2UnKTtcbiAgb3V0cHV0UmFuZ2UgPSBvdXRwdXRSYW5nZS5tYXAoY29sb3JUb1JnYmEpO1xuICBjaGVja1BhdHRlcm4ob3V0cHV0UmFuZ2UpO1xuXG4gIC8vIFsncmdiYSgwLCAxMDAsIDIwMCwgMCknLCAncmdiYSg1MCwgMTUwLCAyNTAsIDAuNSknXVxuICAvLyAtPlxuICAvLyBbXG4gIC8vICAgWzAsIDUwXSxcbiAgLy8gICBbMTAwLCAxNTBdLFxuICAvLyAgIFsyMDAsIDI1MF0sXG4gIC8vICAgWzAsIDAuNV0sXG4gIC8vIF1cbiAgLyogJEZsb3dGaXhNZSg+PTAuMTguMCk6IGBvdXRwdXRSYW5nZVswXS5tYXRjaCgpYCBjYW4gcmV0dXJuIGBudWxsYC4gTmVlZCB0b1xuICAgKiBndWFyZCBhZ2FpbnN0IHRoaXMgcG9zc2liaWxpdHkuXG4gICAqL1xuICBjb25zdCBvdXRwdXRSYW5nZXMgPSBvdXRwdXRSYW5nZVswXS5tYXRjaChzdHJpbmdTaGFwZVJlZ2V4KS5tYXAoKCkgPT4gW10pO1xuICBvdXRwdXRSYW5nZS5mb3JFYWNoKHZhbHVlID0+IHtcbiAgICAvKiAkRmxvd0ZpeE1lKD49MC4xOC4wKTogYHZhbHVlLm1hdGNoKClgIGNhbiByZXR1cm4gYG51bGxgLiBOZWVkIHRvIGd1YXJkXG4gICAgICogYWdhaW5zdCB0aGlzIHBvc3NpYmlsaXR5LlxuICAgICAqL1xuICAgIHZhbHVlLm1hdGNoKHN0cmluZ1NoYXBlUmVnZXgpLmZvckVhY2goKG51bWJlciwgaSkgPT4ge1xuICAgICAgb3V0cHV0UmFuZ2VzW2ldLnB1c2goK251bWJlcik7XG4gICAgfSk7XG4gIH0pO1xuXG4gIC8qICRGbG93Rml4TWUoPj0wLjE4LjApOiBgb3V0cHV0UmFuZ2VbMF0ubWF0Y2goKWAgY2FuIHJldHVybiBgbnVsbGAuIE5lZWQgdG9cbiAgICogZ3VhcmQgYWdhaW5zdCB0aGlzIHBvc3NpYmlsaXR5LlxuICAgKi9cbiAgY29uc3QgaW50ZXJwb2xhdGlvbnMgPSBvdXRwdXRSYW5nZVswXVxuICAgIC5tYXRjaChzdHJpbmdTaGFwZVJlZ2V4KVxuICAgIC5tYXAoKHZhbHVlLCBpKSA9PiB7XG4gICAgICByZXR1cm4gY3JlYXRlSW50ZXJwb2xhdGlvbih7XG4gICAgICAgIC4uLmNvbmZpZyxcbiAgICAgICAgb3V0cHV0UmFuZ2U6IG91dHB1dFJhbmdlc1tpXSxcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gIC8vIHJnYmEgcmVxdWlyZXMgdGhhdCB0aGUgcixnLGIgYXJlIGludGVnZXJzLi4uLiBzbyB3ZSB3YW50IHRvIHJvdW5kIHRoZW0sIGJ1dCB3ZSAqZG9udCogd2FudCB0b1xuICAvLyByb3VuZCB0aGUgb3BhY2l0eSAoNHRoIGNvbHVtbikuXG4gIGNvbnN0IHNob3VsZFJvdW5kID0gaXNSZ2JPclJnYmEob3V0cHV0UmFuZ2VbMF0pO1xuXG4gIHJldHVybiBpbnB1dCA9PiB7XG4gICAgbGV0IGkgPSAwO1xuICAgIC8vICdyZ2JhKDAsIDEwMCwgMjAwLCAwKSdcbiAgICAvLyAtPlxuICAgIC8vICdyZ2JhKCR7aW50ZXJwb2xhdGlvbnNbMF0oaW5wdXQpfSwgJHtpbnRlcnBvbGF0aW9uc1sxXShpbnB1dCl9LCAuLi4nXG4gICAgcmV0dXJuIG91dHB1dFJhbmdlWzBdLnJlcGxhY2Uoc3RyaW5nU2hhcGVSZWdleCwgKCkgPT4ge1xuICAgICAgY29uc3QgdmFsID0gK2ludGVycG9sYXRpb25zW2krK10oaW5wdXQpO1xuICAgICAgY29uc3Qgcm91bmRlZCA9XG4gICAgICAgIHNob3VsZFJvdW5kICYmIGkgPCA0ID8gTWF0aC5yb3VuZCh2YWwpIDogTWF0aC5yb3VuZCh2YWwgKiAxMDAwKSAvIDEwMDA7XG4gICAgICByZXR1cm4gU3RyaW5nKHJvdW5kZWQpO1xuICAgIH0pO1xuICB9O1xufVxuXG5mdW5jdGlvbiBpc1JnYk9yUmdiYShyYW5nZSkge1xuICByZXR1cm4gdHlwZW9mIHJhbmdlID09PSAnc3RyaW5nJyAmJiByYW5nZS5zdGFydHNXaXRoKCdyZ2InKTtcbn1cblxuZnVuY3Rpb24gY2hlY2tQYXR0ZXJuKGFycjogQXJyYXk8c3RyaW5nPikge1xuICBjb25zdCBwYXR0ZXJuID0gYXJyWzBdLnJlcGxhY2Uoc3RyaW5nU2hhcGVSZWdleCwgJycpO1xuICBmb3IgKGxldCBpID0gMTsgaSA8IGFyci5sZW5ndGg7ICsraSkge1xuICAgIGludmFyaWFudChcbiAgICAgIHBhdHRlcm4gPT09IGFycltpXS5yZXBsYWNlKHN0cmluZ1NoYXBlUmVnZXgsICcnKSxcbiAgICAgICdpbnZhbGlkIHBhdHRlcm4gJyArIGFyclswXSArICcgYW5kICcgKyBhcnJbaV0sXG4gICAgKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBmaW5kUmFuZ2UoaW5wdXQ6IG51bWJlciwgaW5wdXRSYW5nZTogQXJyYXk8bnVtYmVyPikge1xuICBsZXQgaTtcbiAgZm9yIChpID0gMTsgaSA8IGlucHV0UmFuZ2UubGVuZ3RoIC0gMTsgKytpKSB7XG4gICAgaWYgKGlucHV0UmFuZ2VbaV0gPj0gaW5wdXQpIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuICByZXR1cm4gaSAtIDE7XG59XG5cbmZ1bmN0aW9uIGNoZWNrVmFsaWRJbnB1dFJhbmdlKGFycjogQXJyYXk8bnVtYmVyPikge1xuICBpbnZhcmlhbnQoYXJyLmxlbmd0aCA+PSAyLCAnaW5wdXRSYW5nZSBtdXN0IGhhdmUgYXQgbGVhc3QgMiBlbGVtZW50cycpO1xuICBmb3IgKGxldCBpID0gMTsgaSA8IGFyci5sZW5ndGg7ICsraSkge1xuICAgIGludmFyaWFudChcbiAgICAgIGFycltpXSA+PSBhcnJbaSAtIDFdLFxuICAgICAgLyogJEZsb3dGaXhNZSg+PTAuMTMuMCkgLSBJbiB0aGUgYWRkaXRpb24gZXhwcmVzc2lvbiBiZWxvdyB0aGlzIGNvbW1lbnQsXG4gICAgICAgKiBvbmUgb3IgYm90aCBvZiB0aGUgb3BlcmFuZHMgbWF5IGJlIHNvbWV0aGluZyB0aGF0IGRvZXNuJ3QgY2xlYW5seVxuICAgICAgICogY29udmVydCB0byBhIHN0cmluZywgbGlrZSB1bmRlZmluZWQsIG51bGwsIGFuZCBvYmplY3QsIGV0Yy4gSWYgeW91IHJlYWxseVxuICAgICAgICogbWVhbiB0aGlzIGltcGxpY2l0IHN0cmluZyBjb252ZXJzaW9uLCB5b3UgY2FuIGRvIHNvbWV0aGluZyBsaWtlXG4gICAgICAgKiBTdHJpbmcobXlUaGluZylcbiAgICAgICAqL1xuICAgICAgJ2lucHV0UmFuZ2UgbXVzdCBiZSBtb25vdG9uaWNhbGx5IG5vbi1kZWNyZWFzaW5nICcgKyBhcnIsXG4gICAgKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBjaGVja0luZmluaXRlUmFuZ2UobmFtZTogc3RyaW5nLCBhcnI6IEFycmF5PG51bWJlcj4pIHtcbiAgaW52YXJpYW50KGFyci5sZW5ndGggPj0gMiwgbmFtZSArICcgbXVzdCBoYXZlIGF0IGxlYXN0IDIgZWxlbWVudHMnKTtcbiAgaW52YXJpYW50KFxuICAgIGFyci5sZW5ndGggIT09IDIgfHwgYXJyWzBdICE9PSAtSW5maW5pdHkgfHwgYXJyWzFdICE9PSBJbmZpbml0eSxcbiAgICAvKiAkRmxvd0ZpeE1lKD49MC4xMy4wKSAtIEluIHRoZSBhZGRpdGlvbiBleHByZXNzaW9uIGJlbG93IHRoaXMgY29tbWVudCxcbiAgICAgKiBvbmUgb3IgYm90aCBvZiB0aGUgb3BlcmFuZHMgbWF5IGJlIHNvbWV0aGluZyB0aGF0IGRvZXNuJ3QgY2xlYW5seSBjb252ZXJ0XG4gICAgICogdG8gYSBzdHJpbmcsIGxpa2UgdW5kZWZpbmVkLCBudWxsLCBhbmQgb2JqZWN0LCBldGMuIElmIHlvdSByZWFsbHkgbWVhblxuICAgICAqIHRoaXMgaW1wbGljaXQgc3RyaW5nIGNvbnZlcnNpb24sIHlvdSBjYW4gZG8gc29tZXRoaW5nIGxpa2VcbiAgICAgKiBTdHJpbmcobXlUaGluZylcbiAgICAgKi9cbiAgICBuYW1lICsgJ2Nhbm5vdCBiZSBdLWluZmluaXR5OytpbmZpbml0eVsgJyArIGFycixcbiAgKTtcbn1cblxuY2xhc3MgQW5pbWF0ZWRJbnRlcnBvbGF0aW9uIGV4dGVuZHMgQW5pbWF0ZWRXaXRoQ2hpbGRyZW4ge1xuICAvLyBFeHBvcnQgZm9yIHRlc3RpbmcuXG4gIHN0YXRpYyBfX2NyZWF0ZUludGVycG9sYXRpb24gPSBjcmVhdGVJbnRlcnBvbGF0aW9uO1xuXG4gIF9wYXJlbnQ6IEFuaW1hdGVkTm9kZTtcbiAgX2NvbmZpZzogSW50ZXJwb2xhdGlvbkNvbmZpZ1R5cGU7XG4gIF9pbnRlcnBvbGF0aW9uOiAoaW5wdXQ6IG51bWJlcikgPT4gbnVtYmVyIHwgc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHBhcmVudDogQW5pbWF0ZWROb2RlLCBjb25maWc6IEludGVycG9sYXRpb25Db25maWdUeXBlKSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLl9wYXJlbnQgPSBwYXJlbnQ7XG4gICAgdGhpcy5fY29uZmlnID0gY29uZmlnO1xuICAgIHRoaXMuX2ludGVycG9sYXRpb24gPSBjcmVhdGVJbnRlcnBvbGF0aW9uKGNvbmZpZyk7XG4gIH1cblxuICBfX21ha2VOYXRpdmUoKSB7XG4gICAgdGhpcy5fcGFyZW50Ll9fbWFrZU5hdGl2ZSgpO1xuICAgIHN1cGVyLl9fbWFrZU5hdGl2ZSgpO1xuICB9XG5cbiAgX19nZXRWYWx1ZSgpOiBudW1iZXIgfCBzdHJpbmcge1xuICAgIGNvbnN0IHBhcmVudFZhbHVlOiBudW1iZXIgPSB0aGlzLl9wYXJlbnQuX19nZXRWYWx1ZSgpO1xuICAgIGludmFyaWFudChcbiAgICAgIHR5cGVvZiBwYXJlbnRWYWx1ZSA9PT0gJ251bWJlcicsXG4gICAgICAnQ2Fubm90IGludGVycG9sYXRlIGFuIGlucHV0IHdoaWNoIGlzIG5vdCBhIG51bWJlci4nLFxuICAgICk7XG4gICAgcmV0dXJuIHRoaXMuX2ludGVycG9sYXRpb24ocGFyZW50VmFsdWUpO1xuICB9XG5cbiAgaW50ZXJwb2xhdGUoY29uZmlnOiBJbnRlcnBvbGF0aW9uQ29uZmlnVHlwZSk6IEFuaW1hdGVkSW50ZXJwb2xhdGlvbiB7XG4gICAgcmV0dXJuIG5ldyBBbmltYXRlZEludGVycG9sYXRpb24odGhpcywgY29uZmlnKTtcbiAgfVxuXG4gIF9fYXR0YWNoKCk6IHZvaWQge1xuICAgIHRoaXMuX3BhcmVudC5fX2FkZENoaWxkKHRoaXMpO1xuICB9XG5cbiAgX19kZXRhY2goKTogdm9pZCB7XG4gICAgdGhpcy5fcGFyZW50Ll9fcmVtb3ZlQ2hpbGQodGhpcyk7XG4gICAgc3VwZXIuX19kZXRhY2goKTtcbiAgfVxuXG4gIF9fdHJhbnNmb3JtRGF0YVR5cGUocmFuZ2U6IEFycmF5PGFueT4pIHtcbiAgICAvLyBDaGFuZ2UgdGhlIHN0cmluZyBhcnJheSB0eXBlIHRvIG51bWJlciBhcnJheVxuICAgIC8vIFNvIHdlIGNhbiByZXVzZSB0aGUgc2FtZSBsb2dpYyBpbiBpT1MgYW5kIEFuZHJvaWQgcGxhdGZvcm1cbiAgICAvKiAkRmxvd0ZpeE1lKD49MC43MC4wIHNpdGU9cmVhY3RfbmF0aXZlX2ZiKSBUaGlzIGNvbW1lbnQgc3VwcHJlc3NlcyBhblxuICAgICAqIGVycm9yIGZvdW5kIHdoZW4gRmxvdyB2MC43MCB3YXMgZGVwbG95ZWQuIFRvIHNlZSB0aGUgZXJyb3IgZGVsZXRlIHRoaXNcbiAgICAgKiBjb21tZW50IGFuZCBydW4gRmxvdy4gKi9cbiAgICByZXR1cm4gcmFuZ2UubWFwKGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICBpZiAodHlwZW9mIHZhbHVlICE9PSAnc3RyaW5nJykge1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICB9XG4gICAgICBpZiAoL2RlZyQvLnRlc3QodmFsdWUpKSB7XG4gICAgICAgIGNvbnN0IGRlZ3JlZXMgPSBwYXJzZUZsb2F0KHZhbHVlKSB8fCAwO1xuICAgICAgICBjb25zdCByYWRpYW5zID0gKGRlZ3JlZXMgKiBNYXRoLlBJKSAvIDE4MC4wO1xuICAgICAgICByZXR1cm4gcmFkaWFucztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEFzc3VtZSByYWRpYW5zXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHZhbHVlKSB8fCAwO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgX19nZXROYXRpdmVDb25maWcoKTogYW55IHtcbiAgICBpZiAoX19ERVZfXykge1xuICAgICAgTmF0aXZlQW5pbWF0ZWRIZWxwZXIudmFsaWRhdGVJbnRlcnBvbGF0aW9uKHRoaXMuX2NvbmZpZyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgIGlucHV0UmFuZ2U6IHRoaXMuX2NvbmZpZy5pbnB1dFJhbmdlLFxuICAgICAgLy8gT25seSB0aGUgYG91dHB1dFJhbmdlYCBjYW4gY29udGFpbiBzdHJpbmdzIHNvIHdlIGRvbid0IG5lZWQgdG8gdHJhbnNmb3JtIGBpbnB1dFJhbmdlYCBoZXJlXG4gICAgICBvdXRwdXRSYW5nZTogdGhpcy5fX3RyYW5zZm9ybURhdGFUeXBlKHRoaXMuX2NvbmZpZy5vdXRwdXRSYW5nZSksXG4gICAgICBleHRyYXBvbGF0ZUxlZnQ6XG4gICAgICAgIHRoaXMuX2NvbmZpZy5leHRyYXBvbGF0ZUxlZnQgfHwgdGhpcy5fY29uZmlnLmV4dHJhcG9sYXRlIHx8ICdleHRlbmQnLFxuICAgICAgZXh0cmFwb2xhdGVSaWdodDpcbiAgICAgICAgdGhpcy5fY29uZmlnLmV4dHJhcG9sYXRlUmlnaHQgfHwgdGhpcy5fY29uZmlnLmV4dHJhcG9sYXRlIHx8ICdleHRlbmQnLFxuICAgICAgdHlwZTogJ2ludGVycG9sYXRpb24nLFxuICAgIH07XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBBbmltYXRlZEludGVycG9sYXRpb247XG4iXX0=