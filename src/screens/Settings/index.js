import React, { Component } from "react";
import { View, Text, Button } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import BulbIcon from '../../components/BulbIdea/index'

import styles from "./Styles";

const bulbIcon = <Icon name="lightbulb-on" size={40} color="#199" />;
const firstColor = "#777";
const secondColor = "#411";

export default class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
      isTurnOn: false,
      counter: 0, 
      iconColor : firstColor
    };

    this.interval = setInterval(
      () =>
      // {
        // if(this.state.props.isTurnOn){
          this.setState(previewState => ({
            isTurnOn: !previewState.isTurnOn,
            counter: ++previewState.counter,
            iconColor : previewState.isTurnOn ? firstColor : secondColor
          }))
        // }else{
          // this.setState(previewState => ({
          //   isTurnOn: !previewState.isTurnOn,
          //   counter: ++previewState.counter,
          //   iconColor : secondColor
          // }))
        // }}
        ,
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  static navigationOption = {
    title: "Home Screen"
  };

  render() {
    // if (this.state.isTurnOn) {
    //   this.setState(previewState => ({
    //       iconColor : firstColor
    //   })) 
    // } else {
    //   this.setState(previewState => ({
    //     iconColor : secondColor
    // })) 
    // }

    return (
      <View style={styles.container}>
        {bulbIcon}
        <Icon name="lightbulb-on" color={this.state.iconColor} size={33}/>
        <BulbIcon colorIcon={this.state.iconColor} name="lightbulb-on" sizeIcon={25}/>
        <Text style={styles.welcome}>Setting page {this.state.counter} and {this.state.iconColor}</Text>
      </View>
    );
  }
}
