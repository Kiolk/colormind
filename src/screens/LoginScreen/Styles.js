import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container :{
        flex :1,
        justifyContent : 'flex-start',
        // backgroundColor : '#fff233',
        flexDirection : 'column',
        // flexWrap: 'wrap',
    },
    welcome:{
        height : '30%',
        // backgroundColor: "#845332",
        justifyContent : 'center',
        alignItems: 'center',
    },
    welcomeTitle:{
        fontSize: 50,
        color: "#aaaaaa",
    },
    inputtedArea :{
        height : '50%',
        // backgroundColor: 'red',
        flex: 1,
        padding : 20,
        alignItems : 'stretch',
        flexDirection : 'column'
    },
    loginButtons: {
        height : '20%',
        flex: 1,
        justifyContent : 'center',
        flexDirection : 'row',
        alignItems: 'center',
        // backgroundColor : 'red',
    }, 
    title :{
        textAlign : 'center',
        fontSize: 33, 
        // textColor : "#888"
    },
    button : {
        margin: 10,
        width : 100,
        fontSize : 22,
        alignItems: 'stretch',
        // backgroundColor : 
    },
    inputTextField : {
        flex : 1,
        width : 200
        // padding : 22
    }
})