import BaseController from "../common/BaseController";
import StorageManager from "../../utils/managers/StorageManager";
import { RegularExpressions } from "../../config/Constants";
export default class LoginScreenController extends BaseController {
  initialSetupView() {
    this.storageManager = new StorageManager();
    // this.view.changeTextValue("^^^^^^^");
  }

  buttonPressHandler() {
    this.view.changeStateTextValue("\\\\\\\\");
  }

  saveLogin(login) {
    this.storageManager.saveLogin(login);
  }

  getLogin() {
    this.storageManager
      .loadLogin()
      .then(this.showLogin.bind(this))
      .catch(this.showErrorMessage.bind(this));
  }

  showLogin(login) {
    if (!login) {
      this.view.changeStateTextValue("No Data");
    } else {
      this.view.changeStateTextValue(login);
    }
  }

  showErrorMessage() {
    this.view.changeStateTextValue("No Data");
  }

  submitEmail(email) {
    this.userEmail = email;
    console.log(email);
  }

  setEmailSubmitted() {
    this.emailSubmitted = true;
    if (this.emailCorrect(this.userEmail)) {
      this.view.changeInputFieldFocus("email");
      this.emailSubmitted = true;
    } else {
      console.log("email incorrect " + this.userEmail);
      this.emailSubmitted = false;
      this.view.setWrongEmail();
    }
  }

  emailCorrect(email) {
    return RegularExpressions.EMAIL.test(email);
  }

  passwordCorrect(password) {
    return password.length > 1;
  }

  submitPassword() {
    console.log("submitPassword in controller");
    if (this.passwordCorrect(this.userPassword)) {
      this.passwordSubmitted = true;
      this.view.changeInputFieldFocus("password");
    } else {
      this.passwordSubmitted = false;
      this.view.setWrongPassword("Password less than 10 symbols");
    }
  }

  onPasswordChange(password) {
    this.userPassword = password;
  }

  addNewUser() {
    if (
      this.emailCorrect(this.userEmail) &&
      this.passwordCorrect(this.userPassword)
    ) {
      const user = {
        userEmail: this.userEmail,
        userPassword: this.userPassword
      };
      this.storageManager
        .saveUser(this.userEmail, user)
        .then(this.successLogin.bind(this))
        .catch(this.errorLogin.bind(this));
    } else {
      console.log("Other happen some incorrect");
    }
  }

  loginExistingUser() {
    if (
      this.emailCorrect(this.userEmail) &&
      this.passwordCorrect(this.userPassword)
    ) {
      const user = {
        userEmail: this.userEmail,
        userPassword: this.userPassword
      };
      console.log("userEmail " + this.userEmail + " userPassword " + this.userPassword);
      
      this.storageManager
        .getUser(this.userEmail)
        .then(this.checkUserExist.bind(this))
        .catch(this.errorLogin.bind(this));
    } else {
      console.log("Other happen");
    }
  }

  checkUserExist(user) {
    console.log(user);
    // const userItem = user[0];
    if (user.userPassword !== this.userPassword){
      this.errorLogin("Not logged");
    }else{
      this.successLogin();
    }
  }

  successLogin() {
    this.storageManager.setIsLogin(true)
    console.log("success call");
    this.view.openNext();
  }

  errorLogin(message) {
    this.storageManager.setIsLogin(false)
    console.log("error " + { message });
    this.view.showError(message);
  }
}
