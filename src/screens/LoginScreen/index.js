import React, { Component } from "react";
import { View, Button, Text } from "react-native";
import Styles from "./Styles";
import { StackActions, NavigationActions } from "react-navigation";

import InputField from "../../components/InputField";
import LoginScreenController from "./LoginScreenController";
import TextWithHint from "../../components/TextWithHint";

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    console.log("Constructor of Login screen");
    this.state = {
      textValue: "*****",
      inputtedText: ""
    };
    console.log("Constructor of Login sscreen");
    this.controller = new LoginScreenController(this);
    this.controller.initialSetupView();
  }

  // componentWillMount() {
  //   console.log(this);
  // }

  changeTextValue(newValue) {
    this.setState({
      textValue: newValue
    });
  }

  changeStateTextValue(newValue) {
    this.setState({
      textValue: newValue
    });
  }

  buttonPress() {
    this.controller.buttonPressHandler();
  }

  saveLogin() {
    console.log("Save");
    this.controller.saveLogin("Vasya");
  }

  showLogin() {
    this.controller.getLogin();
  }

  saveFromInput() {
    // let value = this.inputField.getText();
    this.controller.saveLogin(this.state.inputtedText);
  }

  updateInputtedText(text) {
    this.setState({
      inputtedText: text
    });
  }

  registerNewUser() {
    this.controller.addNewUser();
  }

  logIn() {
    this.controller.loginExistingUser();
  }

  updateEmail(email) {
    console.log("updateEmail");
    this.controller.submitEmail(email);
  }

  setWrongEmail() {
    this.inputField.setError("Incorrect email format");
    this.passwordField.focus();
  }

  setWrongPassword(message) {
    this.passwordField.setError(message);
  }

  onEmailSubmitted() {
    console.log("onEmailSubmitted");
    this.controller.setEmailSubmitted();
  }

  changeInputFieldFocus(field) {
    switch (field) {
      case "email":
        this.passwordField.focus();
        this.inputField.resetError();
        console.log("email pass");
        break;
      case "password":
        this.inputField.focus();
        this.passwordField.resetError();
        console.log("password pass");
        break;
    }
  }

  onPasswordChange(password) {
    this.controller.onPasswordChange(password);
  }

  onPasswordSubmitted() {
    this.controller.submitPassword();
  }

  openNext() {
    this.props.navigation.navigate("BaseStack");
  }

  showError(message) {
    console.log(message);
  }

  render() {
    console.log("render login screen");

    return (
      <View style={Styles.container}>
        <View style={Styles.welcome}>
          <Text style={Styles.welcomeTitle}>Welcome!</Text>
        </View>
        <View style={[Styles.welcome, Styles.inputtedArea]}>
          <InputField
            label="Input email"
            style={Styles.inputTextField}
            keyboardType="email-address"
            returnKeyType="next"
            secureTextEntry={false}
            ref={ref => (this.inputField = ref)}
            onTextSubmitted={this.onEmailSubmitted.bind(this)}
            onTextChange={this.updateEmail.bind(this)}
          />
          <InputField
            label="Input password"
            style={Styles.inputTextField}
            returnKeyType="next"
            keyboardType="default"
            secureTextEntry={true}
            ref={ref => (this.passwordField = ref)}
            onTextSubmitted={this.onPasswordSubmitted.bind(this)}
            onTextChange={this.onPasswordChange.bind(this)}
          />
          <TextWithHint
            text={"Text with hint"}
            hintText={
              "Some text that will show in pop menu. Some text that will show in pop menu. Some text that will show in pop menu. "
            }
          />
        </View>
        <View style={Styles.loginButtons}>
          <View style={Styles.button}>
            <Button
              title="Register"
              onPress={this.registerNewUser.bind(this)}
            />
          </View>
          <View style={Styles.button}>
            <Button title="Login" onPress={this.logIn.bind(this)} />
          </View>
        </View>
      </View>
    );
  }

  // open(){
  //   this.props.navigation.navigate('BaseStack');
  // }

  // render(){
  //   return(<View style={Styles.container}>
  //     <Button title="open main" onPress={this.open.bind(this)}/>
  //   </View>)
  // }
}
