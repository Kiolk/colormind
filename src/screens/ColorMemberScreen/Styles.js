import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        flex : 1,
        backgroundColor : "#324556",
        justifyContent  : "flex-end"
    }, 
    colorArea : {
        height : '60%',
        justifyContent  : "center",
        alignItems: 'center',
        backgroundColor : "#dddd"
    }
})