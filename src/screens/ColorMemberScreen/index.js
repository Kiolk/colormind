import React, { Components } from "react";
import { View } from "react-native";
import Styles from "./Styles";
import Grid from "../../components/Grid";
import Dimensions from "Dimensions";
import ColorMemberScreenController from './ColorMemberScreenController';

export default class ColorMemberScreen extends React.Component {
    
    constructor(props){
        super(props)
        this.colorController = new ColorMemberScreenController(this);
        this.colorController.init();
    }

    render() {
    const {columns, rows, colors} = this.colorController.getGameSettings();
    const height = Dimensions.get("window").height * 0.6;
    const width = Dimensions.get("window").width;
    return (
      <View style={Styles.container}>
        <View />
        <View style={Styles.colorArea} onLayout={event => {}}>
          <Grid height={height} width={width} columns={columns} rows={rows} colors={colors} />
        </View>
      </View>
    );
  }
}
