import BaseController from '../common/BaseController';
import { generateColor } from "../../utils/ColorGenerator";

export default class ColorMemberScreenController extends BaseController{


    init(){
        const colors = []
        const rows = 3;
        const columns = 3;
        const colorsNum = rows * columns;

        for(let counter = 0; counter < colorsNum; ++counter){
            colors.push(generateColor())
        }
        this.GameSettings = {
            columns : rows,
            rows : columns,
            colors : colors
        }
    }

    getGameSettings(){
        return this.GameSettings;
    }
}