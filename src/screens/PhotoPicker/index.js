import React, { Component } from "react";
import { View, Button, Text } from "react-native";
import Styles from "./Styles";
import ThumbedImage from "../../components/ThumbedImage";
import ImagePicker from "react-native-image-picker";
// import { ImageColorPicker } from "react-native-image-color-picker";

export default class PhotoPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tmpSource: "https://picsum.photos/100/100?image=0",
      imageSource: "https://picsum.photos/1000/1000?image=0",
      changePhoto: true
    };
  }

  selectPhoto() {
    const { changePhoto } = this.state;
    const tmpSource = changePhoto
      ? "https://picsum.photos/100/100?image=1"
      : "https://picsum.photos/100/100?image=0";
    const imageSource = changePhoto
      ? "https://picsum.photos/1000/1000?image=1"
      : "https://picsum.photos/1000/1000?image=0";
    const newChangePhoto = !changePhoto;

    this.setState({
      tmpSource,
      imageSource,
      changePhoto: newChangePhoto
    });

    const options = {
      title: "Select Avatar",
      // customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };

        console.log("this ________ " + source);

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        // var image = new P (response.data);
        // image.width; // Image width in pixels
        // image.height; // Image height in pixels
        // var line;
        // while ((line = image.readLine())) {
        //   for (var x = 0; x < line.length; x++) {
        //     var px = line[x]; // Pixel RGB color as a single numeric value
        //     // white pixel == 0xFFFFFF
        //     console.log("pixel color " + px);

        //   }
        // }

        // var RNFS = require("react-native-fs");

        // RNFS.readDir(RNFS.MainBundlePath) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
        //   .then(result => {
        //     console.log("GOT RESULT", result);

        //     // stat the first file
        //     return Promise.all([RNFS.stat(result[0].path), result[0].path]);
        //   })
        //   .then(statResult => {
        //     if (statResult[0].isFile()) {
        //       // if we have a file, read it
        //       return RNFS.readFile(statResult[1], "utf8");
        //     }

        //     return "no file";
        //   })
        //   .then(contents => {
        //     // log the file contents
        //     console.log(contents);
        //   })
        //   .catch(err => {
        //     console.log(err.message, err.code);
        //   });

        this.setState({
          imageSource: source.uri
        });
      }
    });
  }

  render() {
    const { tmpSource, imageSource } = this.state;
    return (
      <View style={Styles.container}>
        <ThumbedImage
          tmpSource={tmpSource}
          imageSource={imageSource}
          resizeMode={"cover"}
        />
        <Button
          style={Styles.button}
          title="Select picture"
          onPress={this.selectPhoto.bind(this)}
        />
      </View>
    );
  }
}
