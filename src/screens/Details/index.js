import styles from "./Styles";
import React, { Component } from "react";
import { View, Text, Button, Alert, Platform } from "react-native";
import Permissions, { Status } from "react-native-permissions";
import Toaster from "../../native/android/Toast";
import OpenSettings from '../../native/android/OpenSettings';

export default class DetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photoIsGranted: false
    };
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    navigationOptions.headerTitle = "Test2";

    return {
      // title: params ? "Details" : "Details Screen",
      // headerTitle : "Test",
      headerTintColor: "#377",
      headerTitleStyle: {
        fontWeight: "normal"
      }
    };
  };

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      leftButton: this.renderTitle(),
      style: styles.navigationBar
    });
  }

  renderTitle() {
    return <Text style={styles.textStyle}>Hello</Text>;
  }

  async cameraPermissions() {
    const value = await Permissions.check("camera");
    console.log(value + " new value by async");
    this.cameraPermissionRequest(value);
  }

  cameraPermissionRequest(status) {
    switch (status) {
      case "authorized":
        this.showToastMessage("Permission authorized");
        console.log("authorized");
        break;
      case "denied":
        this.showToastMessage("Permission denied");
        Permissions.request("camera").then(response => {
          this.setState(previews => ({
            ...previews,
            photoIsGranted: true
          }));
        });
        break;
      case "restricted":
        this.showToastMessage("Camera permissions Restricted");
        Alert.alert(
          "Can we access to camera?",
          "We  need access camera on your device",
          [
            {
              text: "Open settings",
              onPress: this.openSettings,
              style: "destructive"
            },
            {
              text: "Cancel",
              onPress: this.cancelPermission,
              style: "cancel"
            }
          ]
        );
        break;
      case "undetermined":
        this.showToastMessage("Permission undetermined");
        console.log("undetermined");
        break;
      default:
        break;
    }
  }

  openSettings() {
    OpenSettings.openSettings('Camera settings', (msg) => {console.log("_______" + msg + "_________");
    })
    console.log("Open Settings");
  }

  cancelPermission() {
    console.log("Cancel permission");
  }

  showToastMessage(message) {
    if (Platform.OS === "android") {
      Toaster.showToast(message, Toaster.LONG);
    } else {
      console.log(message);
    }
  }

  render() {
    const { photoIsGranted } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Hello, Details Screen of React-Native
        </Text>
        <Button
          title="Request camera permissions"
          onPress={this.cameraPermissions.bind(this)}
        />
        <Button
          style={styles.buttonFirst}
          title="Request external storage permissions"
          color="#112233"
          onPress={() => this.props.navigation.goBack()}
        />
        <Button
          title="Request array of permissions"
          onPress={() => this.props.navigation.navigate("Details")}
          disabled={true}
        />
        <Button
          title="pressssss popToTop"
          onPress={() => this.props.navigation.popToTop()}
          accessibilityLabel="Learn more"
        />
        <Button
          title="Navigate to settings"
          onPress={() => this.props.navigation.navigate("UserInfo")}
        />
        <Text>Photo permissions granted: {"" + photoIsGranted}</Text>
      </View>
    );
  }
}
