import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  buttonFirst: {
    color: "#554455"
  },
  navigationBar: {
    height: 50,
    backgroundColor: "#444",
  },
  textStyle: {
    marginHorizontal: 24,
    marginTop: 12,
    height: 24,
  }
});
