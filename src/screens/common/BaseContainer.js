import React, { Component } from "react";
import {View} from 'react-native';
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";

export default class BaseContainer extends React.Component {
  onSwipeLeft(state) {
    console.log("Swipe left");
    // this.props.navigation.openDrawer();
  }

  onSwipeRight(state) {
    console.log("Swipe right");
    // this.props.navigation.openDrawer();
  }

  render() {
    const gestureConfig = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };

    return (
      <View style={{flex : 1, backgroundColor : '#000'}}>
        <GestureRecognizer
          config={this.gestureConfig}
          style={{flex : 1}}
          onSwipeLeft={state => this.onSwipeLeft(state)}
          onSwipeRight={state => this.onSwipeRight(state)}
          
        />
      </View>
    );
  }
}
