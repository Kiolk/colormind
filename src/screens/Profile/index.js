import React, { Component } from "react";
import { View, Text, Button } from "react-native";

import styles from "./Styles";
import BaseContainer from '../common/BaseContainer'

export default class ProfileScreen extends React.Component {
  render() {
    return (
      <BaseContainer style={styles.container}>
        <Text style={styles.welcome}>Profile page</Text>
        <Button
          title="Open setthgfings"
          onPress={() => this.props.navigation.navigate("Settings")}
        />
        <Button
          title="Open login"
          onPress={() => this.props.navigation.navigate("Login")}
        />
      </BaseContainer>
    );
  }
}
