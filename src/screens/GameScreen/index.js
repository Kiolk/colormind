import React, { Component } from "react";
import Styles from "./Styles";
import { View, Text, Dimensions } from "react-native";
import ColoredSquare from "../../components/ColoredSquare";
import * as ColorGenerator from "../../utils/ColorGenerator";
import ScoreView from "../../components/ScoreView";
import GameScreenController from "./GameScreenController";
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";
export default class GameScreen extends React.Component {
  constructor(props) {
    super(props);
    const { height, width } = Dimensions.get("window");
    const firstBlock = height / 3;
    const secondBlock = (height / 3) * 2;
    this.state = {
      firstBlock: firstBlock,
      secondBlock: secondBlock
    };

    this.controller = new GameScreenController(this);
    console.log("height " + height + " width " + width);

    // const { navigation } = this.props;
    // navigation.dispatch(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName : "Splash"})]
    //   })
    // );
  }

  onSquareTap(color) {
    this.controller.itemTaped(color);
    // this.counter = ++this.counter;
    // this.props.counter = ++this.props.counter;
    // this.scoreLabel.updateValue(this.props.counter)
    // console.log("on item tupped" + this.counter + " " + this.props.counter);
  }

  updateScore(value) {
    this.scoreLabel.updateValue(value);
    console.log("on item tupped" + value);
  }

  // onSwipeLeft(state) {
  //   console.log("^^^Swipe left");
  //   this.props.navigation.closeDrawer();
  // }

  // onSwipeRight(state) {
  //   console.log("^^^Swipe right");
  //   this.props.navigation.openDrawer();
  // }

  render() {
    const { height, width } = Dimensions.get("window");
    const firstBlock = (height - width) / 2;
    const secondBlock = width;
    console.log("height " + this.state.height + " width " + this.state.width);

    const gestureConfig = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 60
    };

    return (
      <View style={Styles.container}>
        {/* <GestureRecognizer
          config={gestureConfig}
          style={{ flex: 1 }}
          onSwipeLeft={state => this.onSwipeLeft(state)}
          onSwipeRight={state => this.onSwipeRight(state)}
        > */}
          <View style={[Styles.timerHeader]}>
            <ScoreView
              ref={score => (this.scoreLabel = score)}
              labelText={11}
            />
          </View>
          <View
            style={[
              Styles.boardArea,
              { height: secondBlock, width: secondBlock }
            ]}
          >
            <ColoredSquare
              width={secondBlock}
              squareNumber={5}
              tapCallback={this.onSquareTap.bind(this)}
            />
          </View>
        {/* </GestureRecognizer> */}
      </View>
    );
  }
}
