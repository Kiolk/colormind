import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor : "#034284"

    justifyContent: "flex-end",
    // alignItems: "center"
  },
  timerHeader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor : "#03df84"
  },
  boardArea: {
    // backgroundColor : "#031114",
    // padding: '$basePadding',
    // margin: '$basePadding',
    // borderWidth: '$baseBorderWidth',
    borderColor: "#222223",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 44,
    justifyContent: "center",
    alignItems: "center"
  }
});
