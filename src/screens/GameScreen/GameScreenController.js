import BaseController from "../common/BaseController";
import {ColorIdentifier} from '../../utils/ColorInfo'
 
export default class GameScreenController extends BaseController {
  constructor(props) {
    super(props);

    this.score = 0;
  }

  itemTaped(color) {
    ++this.score;
    console.log("Score" + color);
    const colorIdentifier = ColorIdentifier.getInstance();
    const colorName = colorIdentifier.name(color)[1]
    this.view.updateScore({
      labelText : colorName,
      color: color
    });
  }
}
