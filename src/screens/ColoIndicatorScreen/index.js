import React from "react";
import { View, Text, Image } from "react-native";
import Styles from "./Styles";
import ThumbedImage from "../../components/ThumbedImage";
import { Pages } from "react-native-pages";
import {
  PagerTabIndicator,
  IndicatorViewPager,
  PagerTitleIndicator,
  PagerDotIndicator
} from "rn-viewpager";
import GestureRecognizer, {
  swipeDirections
} from "../../../node_modules/react-native-swipe-gestures/index";
export default class ColorIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.gestureConfig = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    this.pictureUri = "https://picsum.photos/100/100?image=0";
    this.pictureUri1 = "https://picsum.photos/100/100?image=1";
    this.pictureUri2 = "https://picsum.photos/100/100?image=2";
    this.state = {
      imageWidth: null,
      imageHeight: null
    };
  }

  componentDidMount() {
    Image.getSize(this.pictureUri, (width, height) =>
      this.setState({
        imageHeight: height,
        imageWidth: width
      })
    );
  }

  swipeLeft(state) {
    console.log("_______Swipe left");
    this.props.navigation.openDrawer();
  }

 swipeRight(state) {
    console.log("_____Swipe right");
  }

  render() {
    const { pictureUri } = this.props;
    // Image.getSize(uri, (width, height) => {uriWidth} = width, uriHeight = height)
    console.log(pictureUri);

    const uri = "https://picsum.photos/1000/1000?image=0";
    const uri1 = "https://picsum.photos/1000/1000?image=1";
    const uri2 = "https://picsum.photos/1000/1000?image=2";
    return (
      <GestureRecognizer
        config={this.gestureConfig}
        style={Styles.container}
        // onSwipeRight={state => this.swipeRight(state)}
        onSwipeLeft={state => this.swipeLeft(state)}
      >
        <View style={Styles.image}>
          <Pages>
            <View style={Styles.singlePage}>
              <ThumbedImage
                tmpSource={this.pictureUri}
                imageSource={uri}
                resizeMode={"cover"}
              />
            </View>
            <View style={Styles.singlePage}>
              <ThumbedImage
                tmpSource={this.pictureUri1}
                imageSource={uri1}
                resizeMode={"cover"}
              />
            </View>
            <View style={Styles.singlePage}>
              <ThumbedImage
                tmpSource={this.pictureUri2}
                imageSource={uri2}
                resizeMode={"cover"}
              />
            </View>
          </Pages>
        </View>
        <Text>{this.state.imageHeight} px</Text>
        <Text>{this.state.imageWidth} px</Text>
      </GestureRecognizer>
    );
  }
}
