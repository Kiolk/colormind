import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container : {
        // height : '50%',
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#444'
    },

    image:{
        // flex : 1,
        height: '50%',
        width: '100%',
        backgroundColor: "#009"
    },
    singlePage:{
        height: '100%',
        width : '100%'
    }
})