import React, { Component } from "react";
import { View, Text, Button } from "react-native";
import Styles from "./Styles";
import HttpClient from "../../data/clients/HttpClient";
import FindText from "../../components/FindText";

export default class UserInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      response: "",
      counter: 0
    };

    this.interval = setInterval(
      () =>
        this.setState(previewState => ({
          counter: ++previewState.counter
        })),
      1000
    );
  }

  getCallToAction() {
    HttpClient.get("/Kiolk").then(responseInfo => {
      this.setState({
        response: responseInfo.data.login
      });
      const value = this.refs.text;
      value.toUpper;
    });
    // let inputedText = this.refs['inputtedText'].state.
  }

  updateUserTitle() {
    this.setState({
      response: "test"
    })
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getJson() {}

  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.welcome}>
          Hi "{this.state.response} {this.state.counter}"
        </Text>
        <Button
          style={Styles.container}
          title="get my login"
          onPress={() => this.getCallToAction()}
        />
        <FindText triggerSetText={this.setState({response : "test"})} style={Styles.container} ref="text" />
      </View>
    );
  }
}
