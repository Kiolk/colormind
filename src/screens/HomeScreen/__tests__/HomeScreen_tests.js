import React from 'react';
import render from 'react-test-renderer';
import HomeScreen from '../index';

test("Correct render test", () => {
    const component = render.create(<HomeScreen/>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});