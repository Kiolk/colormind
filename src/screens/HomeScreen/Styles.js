import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container: {
      flex: 1,
      flexDirection : 'column',
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#FFF"
    },
    welcome: {
      fontSize: 20,
      textAlign: "center",
      margin: 10
    },
    instructions: {
      textAlign: "center",
      color: "#333333",
      marginBottom: 5
    },
    buttonFirst: {
      color: "#554455"
    },
    navigationBar: {
        elevation: 0
    }
  });
