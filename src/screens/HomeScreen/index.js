import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  DeviceEventEmitter,
  findNodeHandle,
  Platform,
  UIManager
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import ImageView from "../../native/android/ImageView";

import styles from "./Styles";

const icon = <Icon name="ios-book" size={50} color="#8f8" />;
const iconButton = (
  <Icon.Button name="ios-book" background="#333">
    Test button
  </Icon.Button>
);

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "Text from react native part"
    };
  }

  componentDidMount() {
    console.log("Did mount call");
    this.eventListener = DeviceEventEmitter.addListener("pressOn", e =>
      console.log(e.data)
    );
  }

  componentWillUnmount() {
    console.log("Did unmount call");
    DeviceEventEmitter.removeListener(this.eventListener);
  }

  changeNativeView() {
    console.log("Call change native component");
    this.onPressFirstCommand();
    this.setState(
      previews =>
        (this.state = {
          ...previews,
          text: "Change native component from react by press"
        })
    );
  }

  pressNativeButton(message) {
    console.log(message.nativeEvent.data + "++++++");
  }

  async onPressFirstCommand() {
    
    const handle = findNodeHandle(this.image);
    if (!handle) {
      throw new Error("Component not find");
    }

    console.log("First call" + handle);

    await Platform.select({
      android: async () => {
        console.log("Android call first");
        
        return UIManager.dispatchViewManagerCommand(
          handle,
          UIManager.getViewManagerConfig('ImageView').Commands.firstCommand,
          []
        );
      },
      ios: async () => {}
    })();
  }

  async onPressSecondCommand() {
    console.log("Second call");
    
    const handle = findNodeHandle(this.image);
    if (!handle) {
      throw new Error("Component not find");
    }

    await Platform.select({
      android: async () => {
        console.log("Android call second" + handle);
        
        return UIManager.dispatchViewManagerCommand(
          handle,
          UIManager.getViewManagerConfig('ImageView').Commands.secondCommand,
          []
        );
      },
      ios: async () => {}
    })();
  }

  render() {
    const { text } = this.state;
    console.log(text);

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Hello, React-Native!</Text>
        <Icon name="ios-bulb" color="#f34" size={100} />
        {icon}
        {iconButton}
        <Button title="Press first command" onPress={() => this.changeNativeView()} />
        <Button title="Press second command" onPress={() => this.onPressSecondCommand()} />
        <View
          style={{
            flex: 1,
            height: 200,
            width: "100%",
            backgroundColor: "#443564",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ImageView
            ref={(ref) => {
              this.image = ref
            }}
            style={{ height: 200, width: "100%" }}
            text={text}
            onChange={this.pressNativeButton.bind(this)}
          />
        </View>
      </View>
    );
  }
}
