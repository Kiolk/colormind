import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  splashContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  iconStyle: {
    // alignContent : 'center',
    // alignItems: 'center',
    width : 200,
    textAlign: "center",
  }
});
