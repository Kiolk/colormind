import BaseController from "../common/BaseController";
import StorageManager from "../../utils/managers/StorageManager";
import { ViewPagerAndroid } from "react-native-gesture-handler";

export default class SplashScreenController extends BaseController {
  constructor(props) {
    super(props);
    this.storageManager = new StorageManager();
  }

  checkUserLogin() {
    this.storageManager
      .getIsLogin()
      .then(this.resolveUserLogin.bind(this))
      .catch(this.errorGettingInformation.bind(this));
  }

  resolveUserLogin(isLogged) {
    if (isLogged) {
      console.log("User was login" + isLogged);
      this.view.moveToMainScreen();
    } else {
      console.log("User wasn't login before" + isLogged);
      this.view.moveToRegistrationScreen();
    }
  }

  errorGettingInformation(error) {
    console.log("Error by getting information " + error.name);
    switch (error.name) {
      case "NotFoundError":
        this.view.moveToRegistrationScreen();
        break;
      case "ExpiredError":
        this.view.moveToRegistrationScreen();
        break;
      default:
        this.view.showErrorMessage(error.name);
        break;
    }
  }
}
