import React, { Comment } from "react";
import { View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Styles from "./Styles";
import { StackActions, NavigationActions } from "react-navigation";
import SplashScreenController from "./SplashScreenController";

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    console.log("SplashScreen start");

    this.state = {
      color: "#000000",
      timerCount: 0
    };

    this.interval = setInterval(() => this.updateSplashScreen(), 1000);

    this.controller = new SplashScreenController(this);
  }

  updateSplashScreen() {
    if (this.state.timerCount == 5) {
      this.checkUserLogin();
      clearInterval(this.interval);
    } else if (this.state.timerCount > 5) {
      return;
    } else {
      this.setState(previews => ({
        color: this.generateRandomColor(),
        timerCount: ++previews.timerCount
      }));
    }
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
    clearInterval(this.interval);
  }

  checkUserLogin() {
    // this.moveToMainScreen() ;
    // this.moveToRegistrationScreen();
    this.controller.checkUserLogin();
  }

  generateRandomColor() {
    const color =
      "#" +
      Math.random()
        .toString(16)
        .slice(2, 8);
    console.log("Generated color: " + color);
    return color;
  }

  moveToMainScreen() {
    const { navigation } = this.props;
    clearInterval(this.interval);
    this.props.navigation.navigate("BaseStack");
    // navigation.dispatch(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName: "BaseStack" })]
    //   })
    // );
  }

  moveToRegistrationScreen() {
    console.log(" moveToRegistrationScreen");
    clearInterval(this.interval);
    this.props.navigation.navigate("LoginScreen");

    // const { navigation } = this.props;
    // navigation.dispatch(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName: 'Registration'})]
    //   })
    // );
  }

  showErrorMessage(error) {}

  render() {
    const { color } = this.state;
    return (
      <View style={Styles.splashContainer}>
        <Icon name="brain" style={Styles.iconStyle} size={100} color={color} />
      </View>
    );
  }
}
