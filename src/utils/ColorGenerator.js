import React, { Comment } from "react";
import ColorInfo, {ColorIdentifier} from '../utils/ColorInfo';

export const generateColor = () => {
    const color =
      "#" +
      Math.random()
        .toString(16)
        .slice(2, 8);
    console.log("Generated color: " + color);
    const info = ColorIdentifier.getInstance();
    const value = info.name(color);
    console.log(value[1]);
    return color;
  }