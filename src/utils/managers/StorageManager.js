import Storage from "react-native-storage";
import { AsyncStorage } from "react-native";
import { value } from "react-native-extended-stylesheet";

export default class StorageManager {
  constructor() {
    this.storage = new Storage({
      size: 1000,
      storageBackend: AsyncStorage,
      defaultExpires: null,
      enableCache: true
    });
  }

  saveLogin(value) {
    return this.storage.save({
      key: "login",
      data: value,
      expires: null
    });
  }

  loadLogin() {
    return this.storage.load({
      key: "login"
    });
  }

  getUser(key) {
    console.log("load user " + { key });
    return this.storage.load({
      key: key
    });
  }

  saveUser(key, user) {
    console.log("save user " + { key } + " " + { user });
    return this.storage.save({
      key: key,
      data: user,
      expires: null
    });
  }

  setIsLogin(isLogged) {
    return this.storage.save({
      key: 'isLogin',
      data : isLogged,
      expires: 1000 * 60 * 10
    })
  }

  getIsLogin() {
    return this.storage.load({
      key: 'isLogin'
    })
  }
}
