/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { StatusBar, View, SafeAreaView, StyleSheet } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import AppContainer from "../src/config/Routes";
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";
import BaseContainer from "../src/screens/common/BaseContainer";
import { MenuProvider } from "react-native-popup-menu";

EStyleSheet.build({
  $baseTextColor: "#848689",
  $baseIconColor: "#3a3c3f",
  $basePadding: 5,
  $baseBorderWidth: 6
});

export default class App extends Component {
  onSwipeLeft(state) {
    console.log("Swipe left");
    // this.screen.closeDrawer()
    // this.props.navigation.openDrawer();
  }

  onSwipeRight(state) {
    console.log("Swipe right");
    // this.props.navigation.openDrawer();
    // this.screen.openDrawer()
  }

  render() {
    const gestureConfig = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    return (
      <MenuProvider>
        <View style={[styles.container]}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          <SafeAreaView style={styles.safeArea}>
            <AppContainer ref={ref => (this.screen = ref)} />
          </SafeAreaView>
        </View>
      </MenuProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  safeArea: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  }
});
