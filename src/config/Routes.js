import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator,
  createDrawerNavigator,
  DrawerNavigator,
  createSwitchNavigator
} from "react-navigation";

import HomeScreen from "../screens/HomeScreen";
import DetailsScreen from "../screens/Details";
import SettingsScreen from "../screens/Settings";
import ProfileScreen from "../screens/Profile";
import UserInformation from "../screens/UserInformation";
import LoginScreen from "../screens/LoginScreen";
import SplashScreen from "../screens/SplashScreen";
import GameScreen from "../screens/GameScreen";
import ColorIndicator from "../screens/ColoIndicatorScreen";
import PhotoPicker from "../screens/PhotoPicker";
import DrawerMenu from "../components/DrawerMenu/DrawerMenu";
import ColorMemberScreen from '../screens/ColorMemberScreen';

// const HomeStack = createStackNavigator(
//   {
//     Home: {
//       screen: HomeScreen,
//       navigationOptions: {
//         title: "Home Screen"
//       }
//     },
//     Details: {
//       screen: DetailsScreen,
//       navigationOptions: {},
//       navigationOptions: {
//         title: "Details"
//       }
//     },
//     UserInfo: {
//       screen: UserInformation,
//       navigationOptions: {
//         header: null
//       }
//     }
//   },
//   {
//     initialRouteName: "Home",
//     defaultNavigationOptions: {
//       headerStyle: {
//         backgroundColor: "#853"
//       },
//       headerTintColor: "#fff",
//       headerTitleStyle: {
//         fontWeight: "bold"
//       },
//       tabBarIcon: ({ focused, horizontal, tintColor }) => {
//         return <Icon name="lightbulb-on" color={"#333"} size={33} />;
//       }
//     }
//   }
// );

const LoginStack = createStackNavigator({
  Splash: {
    screen: SplashScreen
  },
  Registration: {
    screen: LoginScreen
  }
},{
  header : null,
  initialRouteName: 'Splash',
  // defaultNavigationOptions : {
  //   header : null
  // }
});

const BaseStack = createDrawerNavigator(
  {
    Profile: {
      screen: ProfileScreen
    },
    Main: {
      screen: SettingsScreen
    },
    DetailsScreen : {
      screen : DetailsScreen
    },
    Home: {
      screen: HomeScreen
    },
    Game: {
      screen: GameScreen,
    },
    ColorIndicator: {
      screen: ColorIndicator
    },
    PhotoPicker: {
      screen: PhotoPicker
    },
    ColorMember : {
      screen : ColorMemberScreen
    }
  },
  {
    initialRouteName: "Home",
    contentComponent : DrawerMenu
  }
);


const GeneralNavigator = createSwitchNavigator(
  {
    Splash : {screen : SplashScreen},
    LoginScreen: { screen: LoginScreen },
    BaseStack: { screen: BaseStack }
  },
  {
    // headerMode: "none",
    initialRouteName: 'BaseStack'
    // navigationOptions: {
    //   header: {
    //     title : 'Welcome!',
    //     headerTintColor : 'white',
    //     backgroundColor : '#4C3E54'
    //   }
    // }
  }
);

// const SettingStack = createStackNavigator(
//   {
//     Profile: {
//       screen: ProfileScreen
//     },
//     Settings: {
//       screen: SettingsScreen
//     },
//     Login: {
//       screen: LoginScreen,
//       navigationOptions: {
//         header: null,
//         tabBarVisible: false
//       }
//     }
//   },
//   {
//     initialRouteName: "Login",
//     defaultNavigationOptions: {
//       tabBarVisible: false
//     }
//   }
// );

// SettingStack.defaultNavigationOptions = {
//   header: null
// };

// const TabNavigator = createBottomTabNavigator({
//   Settings: SettingStack,
//   Home: HomeStack
// });

// TabNavigator.navigationOptions = {
//   header: null
// };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  buttonFirst: {
    color: "#554455"
  },
  statusBar: {
    height: 48,
    backgroundColor: "#209"
  }
});

export default createAppContainer(GeneralNavigator);
