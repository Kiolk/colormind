const RegularExpressions = {
    // eslint-disable-next-line
    EMAIL: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w*)+$/
  };

const ImageSources = {
  headerImageBackground : 'https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/07/colors_pigments.jpg?itok=JJPhQ3do&fc=50,50'
}

  export {RegularExpressions, ImageSources}