import axios from 'axios';

const instance = axios.create()

instance.defaults.baseURL="https://api.github.com/users";
instance.defaults.timeout=20000;

export default instance


// axios.create({
//     baseURL:"https://api.github.com/users",
//     timeout:2000
// })