import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container : {
        height : 100,
        width : 100,
        // backgroundColor : "#543",
    },
    image:{
        flex: 1,
        borderRadius : 50
    }
})