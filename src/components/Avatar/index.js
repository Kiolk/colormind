import React, { Component } from "react";
import { View, Image, TouchableWithoutFeedback } from "react-native";
import Styles from "./Styles";

export default class Avatar extends React.Component {

    tapOnAvatar(){
        const {avatarTap} =this.props
        // this.props.avatarTap()
        console.log("tap");
        // avatarTap();
        
    }

  render() {
    const { sourceUri } = this.props;
    const { avatarTap } = this.props;
    console.log(avatarTap);
    
    return (
      <View style={Styles.container} onPress>
        {/* <TouchableWithoutFeedback onPress={() => this.tapOnAvatar()}> */}
          <Image style={Styles.image} source={{ uri: sourceUri }} />
        {/* </TouchableWithoutFeedback> */}
      </View>
    );
  }
}
