// import propTypes from "prop-types";
import React, { Component } from "react";
import { View, StatusBar } from "react-native";

import styles from "./Styles";

export default class StatusBarWrapper extends Component {
  render() {
    const { children } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        {children}
      </View>
    );
  }
}

// StatusBarWrapper.propTypes = {
//     children: propTypes.any.isRequired,
// };
