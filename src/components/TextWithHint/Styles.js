import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: "#222345",
    fontSize: 16
    // fontFamily : ""
  },
  image: {
    height: 20,
    width: 20
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 5,
    backgroundColor: "#443",
    borderRadius: 20
  },
  menuPopContainer: {
    paddingLeft: 8,
    paddingRight: 8,
    paddingVertical: 16
  },
  menuItemStyle: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#2345"
  },
  hintText: {
    fontSize: 12,
    lineHeight: 18,
    color: "#7489"
  }
});
