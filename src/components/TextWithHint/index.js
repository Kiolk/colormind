import React, { Component } from "react";
import { View, Text, Image, TouchableWithoutFeedback } from "react-native";
import {
  Menu,
  MenuTrigger,
  MenuOptions,
  renderers
} from "react-native-popup-menu";
import Styles from "./Styles";

export default class TextWithHint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowHint: false
    };
  }

  showPopMenu() {
    this.setState(
      previews => ({
        ...previews,
        isShowHint: true
      })
    );
  }

  hidePopMenu() {
    this.setState(previews => ({
      ...previews,
      isShowHint: false
    }));
  }

  render() {
    const { Popover } = renderers;
    const { text, hintText } = this.props;
    const { isShowHint } = this.state;
    return (
      <View style={Styles.container}>
        <Text style={Styles.text}>{text}</Text>
        <Menu
          renderer={Popover}
          rendererProps={{
            width: 266
          }}
          onClose={this.hidePopMenu.bind(this)}
        >
        <MenuTrigger
          triggerOnPress
          customStyles={{
            TriggerTouchableComponent: TouchableWithoutFeedback
          }}
          onPress={this.showPopMenu.bind(this)}
        >
          <View style={Styles.imageContainer}>
            <Image
              style={Styles.image}
              source={
                isShowHint
                  ? require("../../../assets/drawable/cross.png")
                  : require("../../../assets/drawable/question.png")
              }
            />
          </View>
        </MenuTrigger>
        <MenuOptions
          style={Styles.menuPopContainer}
          optionsContainerStyle={Styles.menuItemStyle}
        >
          <Text style={Styles.hintText}>{hintText}</Text>
        </MenuOptions>
        </Menu>
      </View>
    );
  }
}
