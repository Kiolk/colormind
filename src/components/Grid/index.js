import React, { Components } from "react";
import { View } from "react-native";
// import Styles from './Styles';
import ColorSquare from "../ColorSquare/index";

export default class Grid extends React.Component {

  render() {
    const { height, width, columns, rows, colors} = this.props;
    const singleWidth = width / columns;
    const singleHeight = height / rows;
    const rowViews = [];
    for (let nRow = 0; nRow < rows; ++nRow) {
      const columnView = [];
      for (let nColumn = 0; nColumn < columns; ++nColumn) {
          const index = nRow * columns + nColumn
        columnView.push(
          <ColorSquare
            key={nColumn}
            height={singleHeight}
            width={singleWidth}
            color={colors[index]}
          />
        );
      }
      rowViews.push(
        <View style={{ height: singleHeight, width: width, flexDirection: "row" }} key={nRow}>
          {columnView}
        </View>
      );
    }

    return (
      <View style={{ height: height, flexDirection: "column" }}>
        {rowViews}
      </View>
    );
  }
}
