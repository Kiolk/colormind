import React from "react";
import { View, Image, Text, Animated } from "react-native";
import Styles from "./Styles";
import ImagePicker from 'react-native-image-picker';

export default class ThumbedImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    
  }
  tmpAnimated = new Animated.Value(0);
  imageAnimated = new Animated.Value(0);
  
  tmpImageLoaded = () => {
    console.log("tmp loaded");

    Animated.timing(this.tmpAnimated, {
      toValue: 1
    }).start();
  };

  imageLoaded = () => {
    console.log("image loaded");

    Animated.timing(this.imageAnimated, {
      toValue: 1
    }).start();
  };

  render() {
    const { tmpSource, imageSource, ...props } = this.props;
    console.log("tmp " + tmpSource + " image " + imageSource);
    return (
      <View style={Styles.container}>
        <Animated.Image
          // {...props}
          // style={{height:'100', width : '100'}}
          style={[Styles.imageOverlay, { opacity: this.tmpAnimated }]}
          source={{
            uri: tmpSource,
            headers: {
              Pragma: "no-cache"
            }
          }}
          blurRadius={2}
          onLoad={this.tmpImageLoaded}
        />
        <Animated.Image
          {...props}
          style={[Styles.imageOverlay, { opacity: this.imageAnimated }]}
          source={{
            uri: imageSource,
            headers: {
              Pragma: "no-cache"
            }
          }}
          onLoad={this.imageLoaded}
        />
        {/* <Image
        source={{
            uri : 'https://picsum.photos/1000/600?image=0'
        }}
        /> */}
      </View>
    );
  }
}
