import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "#bbb", 
        justifyContent : "flex-start",
        // alignItems: 'center'
    },
    menuHeader : {
        height : '25%',
        width : '100%',
        justifyContent : "center",
        // padding : 5
        // backgroundColor : "#bbb"
    },
    menuItem:{
        color : "#998",
        fontSize : 15
    },
    imageBackground : {
        flex : 1,
        padding : 5,
        justifyContent : "center",
    },
    buttonStyle : {
        margin : 5,
        // borderRadius : 2,
        borderColor : '#124',
        borderBottomWidth : 1,
        padding : 5 
    }
})