import React, { Component } from "react";
import { View, Text, Button, ImageBackground, TouchableWithoutFeedback } from "react-native";
import Styles from "./Styles";
import { NavigationActions } from "react-navigation";
import Avatar from "../Avatar";
import { ImageSources } from "../../config/Constants";

export default class DrawerMenu extends React.Component {
  navigateToScreen = route => () => {
    console.log("Press on " + route + " " + this.props.navigation.routeName);

    const navigationAction = NavigationActions.navigate({
      routeName: route
    });

    this.props.navigation.navigate(navigationAction);
  };

  logOut = () => () => {
    const navigationAction = NavigationActions.navigate({
      routeName: "LoginScreen"
    });

    this.props.navigation.navigate(navigationAction);
  };

  avatarTap = route => ()  => {
    console.log("avatar tap");
    const navigationAction = NavigationActions.navigate({
      routeName: "PhotoPicker"
    });

    this.props.navigation.navigate(navigationAction);
  }

  render() {
    // const { navigation } = this.props;
    const source = ImageSources.headerImageBackground;
    return (
      <View style={Styles.container}>
        <View style={Styles.menuHeader}>

          <TouchableWithoutFeedback onPress={this.navigateToScreen("PhotoPicker")}>
          <ImageBackground
            style={Styles.imageBackground}
            source={{ uri: source }}
            resizeMode="cover"
          >
            <Avatar
              sourceUri="https://picsum.photos/100/100?image=1"
              avatarTap={this.avatarTap}
            />
          </ImageBackground>
            </TouchableWithoutFeedback>
        </View>
        <View>
          <View style={Styles.buttonStyle}>
            <Text
              style={Styles.menuItem}
              onPress={this.navigateToScreen("Game")}
            >
              Game
            </Text>
          </View>
          <View style={Styles.buttonStyle}>
            <Text
              style={Styles.menuItem}
              onPress={this.navigateToScreen("ColorMember")}
            >
              ColorMember
            </Text>
          </View>
          <View style={Styles.buttonStyle}>
            <Text
              style={Styles.menuItem}
              onPress={this.navigateToScreen("Main")}
            >
              Settings
            </Text>
          </View>
          <View style={Styles.buttonStyle}>
            <Text style={Styles.menuItem} onPress={this.logOut()}>
              Log out
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
