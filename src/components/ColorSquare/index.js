import PropTypes from "prop-types";
import React from "react";
import { View, TouchableWithoutFeedback, Animated } from "react-native";
import Styles from "./Styles";

export default class ColorSquare extends React.Component {
  constructor(props) {
    super(props);

    const { height, width } = this.props;
    const padding = 5;
    const sideSize = Math.min(height, width) - padding * 2;
    const borderRadius = sideSize / 2;

    this.state = {
      fadeAnim: new Animated.Value(0),
      borderRadiusAnim: new Animated.Value(0),
      sideSizeAnim: new Animated.Value(0),
      borderRadius,
      sideSize
    };
    this.isShowed = true;
  }

  componentDidMount() {
    console.log("Component did mount call");
    this.startAnimate();
  }

  onPressSquare() {
    console.log("onPressSquare");
    this.startAnimate();
  }

  startAnimate() {
    console.log("start animate isShow" + this.isShowed);

    if (this.isShowed) {
      console.log("hiding");
      const { borderRadius, sideSize } = this.state;

      Animated.timing(this.state.fadeAnim, {
        toValue: 1,
        duration: 1000
      }).start(this.onComplete.bind(this));

      Animated.timing(this.state.borderRadiusAnim, {
        toValue: borderRadius,
        duration: 1000
      }).start(this.onComplete.bind(this));

      Animated.timing(this.state.sideSizeAnim, {
        toValue: sideSize,
        duration: 1000
      }).start(this.onComplete.bind(this));

      this.isShowed = false;
    } else {
      console.log("showing");

      Animated.timing(this.state.fadeAnim, {
        toValue: 0,
        duration: 1000
      }).start(this.onComplete.bind(this));

      Animated.timing(this.state.borderRadiusAnim, {
        toValue: 0,
        duration: 500
      }).start(this.onComplete.bind(this));
      this.isShowed = true;

      console.log("ss");

      Animated.timing(this.state.sideSizeAnim, {
        toValue: 0,
        duration: 1000
      }).start(this.onComplete.bind(this));
    }
  }

  onComplete() {
    console.log("Animation ended");
  }

  render() {
    console.log("render square");
    const { height, width, color } = this.props;
    // const padding = 5;
    // const sideSize = Math.min(height, width) - padding * 2;
    // const borderRadius = sideSize / 2;
    const { fadeAnim, sideSizeAnim, borderRadiusAnim } = this.state;
    return (
      <TouchableWithoutFeedback onPress={this.onPressSquare.bind(this)}>
        <View
          style={[
            Styles.block,
            {
              backgroundColor: "#098",
              // borderRadius: size,
              // borderColor: "22",
              // elevation: elevation,
              height: height,
              width: width
            }
          ]}
        >
          <Animated.View
            style={{
              height: sideSizeAnim,
              width: sideSizeAnim,
              borderRadius: borderRadiusAnim,
              backgroundColor: color,
              opacity: fadeAnim
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

ColorSquare.propTypes = {
  height : PropTypes.number.isRequired,
  width : PropTypes.number.isRequired
};
