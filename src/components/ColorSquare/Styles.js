import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: "center",
    // alignItems: "center",
    justifyContent: "space-between"
  },
  block: {
    flex: 1,
    // height: 20,
    // width: 20,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "$baseIconColor",
    backgroundColor: "#034284"
  }
});
