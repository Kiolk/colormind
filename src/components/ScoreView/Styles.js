import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'row',
    // backgroundColor: "#376923",
    flexWrap: "wrap",
    // alignSelf: "baseline",
    // justifyContent: "center",
    // alignItems: "center"
  },
  labelDigit: {
    fontSize: 60,
    color: "#444444",
    // alignSelf: "center",
    // justifyContent: "center",
    fontFamily: "AtomicClockRadio",
    // alignItems: "center"
  }
});
