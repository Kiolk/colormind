import React, { Component } from "react";
import { View, Text } from "react-native";
import Styles from "./Styles";

export default class ScoreView extends React.Component {
  constructor(props) {
    super(props);

    const { labelText, } = this.props;

    this.state = {
      labelText: labelText
    };
  }

  updateValue(value){
    const {labelText, color} = value
    console.log("ScoreView " + value);
          this.setState({
        labelText: labelText,
        color: color
      })
  }

  render() {
    const {color} = this.state;
    return (
      <View style={Styles.container}>
        <Text style={[Styles.labelDigit, {color: color}]}>{this.state.labelText}</Text>
      </View>
    );
  }
}
