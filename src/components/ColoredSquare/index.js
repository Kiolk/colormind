import React from "react";
import { View, Text, TouchableWithoutFeedback } from "react-native";
import Styles from "./Styles";
import { generateColor } from "../../utils/ColorGenerator";

export default class ColoredSquare extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { width, squareNumber } = this.props;
    const singleBlockWidth = width / squareNumber;
    const{tapCallback} = this.props;
    const blockStyle = {
      height: singleBlockWidth,
      width: singleBlockWidth
    };


    console.log("width " + width + " number " + squareNumber);

    const rowViews = [];
    for (let i = 0; i < squareNumber; ++i) {
      const columnView = [];
      for (let b = 0; b < squareNumber; ++b) {
        const color = this.generateRandomColor();
        const radius = singleBlockWidth / 2;
        columnView.push(
          <Square
            style={[Styles.singleBlock, blockStyle]}
            key={b}
            borderRadius={radius}
            tapCallback={tapCallback}
          />
        );
      }
      rowViews.push(
        <View
          style={[Styles.columnContainer, { width: singleBlockWidth }]}
          key={i}
        >
          {columnView}
        </View>
      );
    }
    console.log("array of view" + rowViews);

    return <View style={Styles.container}>{rowViews}</View>;
  }

  generateRandomColor() {
    const color =
      "#" +
      Math.random()
        .toString(16)
        .slice(2, 8);
    console.log("Generated color: " + color);
    return color;
  }
}

export class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: generateColor()
    };
  }

  onPressSquare() {
    const {tapCallback} = this.props;
    console.log("onPressSquare");

    tapCallback(this.state.color);

    this.setState({
      // color: generateColor()
      borderColor : "red",
      elevation : 6
    });
  }

  render() {
    const { borderRadius } = this.props;
    const {borderColor, elevation} = this.state;
    const size = borderRadius -5;
    return (
      // <View styles={[{height: size, width : size, flex : 1}] }>
      <TouchableWithoutFeedback onPress={this.onPressSquare.bind(this)}>
        <View
          style={[
            Styles.block,
            { backgroundColor: this.state.color, borderRadius: size, borderColor : borderColor, elevation : elevation }
          ]}
        />
      </TouchableWithoutFeedback>
      // </View>
    );
  }
}
