import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: "center",
    // alignItems: "center",
    justifyContent: "space-between"
  },
  columnContainer: {
    flex: 1,
    flexDirection: "column",
    // justifyContent: "center",
    // alignItems: "center",
    justifyContent: "space-between"
  },
  singleBlock: {
    flex: 1,
    // height: 40,
    // width: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ee3"
  },
  block: {
    flex: 1,
    // height: 20,
    // width: 20,
    borderWidth: 1,
    borderColor: "$baseIconColor",
    backgroundColor: "#034284"
  }
});
