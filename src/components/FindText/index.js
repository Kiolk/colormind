import React from "react";
import { View, Text, TextInput } from "react-native";
import Styles from "./Styles";

export default class FindText extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
  }

  render() {
    return (
      <View style={Styles.container}>
        <TextInput
          style={Styles.placeHolderStyle}
          placeholder="Type your search"
          onChangeText={text => {
            this.setState({ text });
            this.props.triggerSetText;
          }}
        />
        <Text style={Styles.normalText}>{this.state.text}</Text>
      </View>
    );
  }
}
