import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        height: 50,
        width: 200,
        backgroundColor : "#573",
        borderColor: "#221",
        borderRadius: 3,
        borderWidth: 5
    },
    placeHolderStyle:{
        fontSize: 20,
        fontStyle: "italic"
    },
    normalText:{
        fontSize: 25,
        fontStyle: "normal"
    }
})