import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container :{
        height : 50,
        width : 50,
        justifyContent: "center",
        backgroundColor : "red"
    },
    bulb:{
        justifyContent: "center",
        borderColor: "#444",
        textAlign: "center",
        textAlignVertical:"top"
    }
})