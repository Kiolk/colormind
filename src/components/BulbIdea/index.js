import React, { Component } from "react";
import { View, Text } from "react-native";
import styles from "./Styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class BulbIdea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colorIcon: "#222",
      sizeIcon: 44,
      name: "lightbulb-on"
    };
  }

  render() {
    return (
      <View style={styles.container}>
      {/* <View style={styles.container}> */}
        <Icon
          style={styles.bulb}
          justifyContent="justifyContent"
          name={this.props.name}
          size={this.props.sizeIcon}
          color={this.props.colorIcon}
        />
        {/* </View> */}
      </View>
    );
  }
}
