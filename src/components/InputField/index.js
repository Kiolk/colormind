import React from "react";
import { View } from "react-native";
import { TextField } from "react-native-material-textfield";
import Styles from "./Styles";

export default class InputField extends React.Component {
  constructor(props) {
    super(props);
    console.log("Constructor input field");
  }
  state = {
    textValue: ""
  };

  getText() {
    return "Lesha";
  }

  handleTextInput(newText) {
    const { onTextChange } = this.props;
    const { textValue } = this.state;
    const newState = {
      ...this.state,
      textValue: newText
    };
    if (newText.length > 0) {
      this.textField.label = "";
      console.log("newText.length");
    } else {
      this.textField.label = "Input some text";
      console.log("must zero");
    }
    this.setState(() => newState);
    console.log("Text change");
    onTextChange(newText);
  }

  handleTextSubmit() {
    const { onTextSubmitted } = this.props;
    onTextSubmitted();
  }

  setError() {
    this.setState(previews => ({
      ...previews,
      hasError: true,
      errorText: "Incorrect input"
    }));
  }

  focus(){
    this.textField.focus();
    console.log("focus")
  }

  resetError(){
    this.setState(previews => ({
      ...previews,
      hasError: false,
    }));
  }

  render() {
    console.log("render input");
    let { textValue } = this.state;
    const { returnKeyType, keyboardType, label, secureTextEntry } = this.props;
    const { hasError, errorText } = this.state;
    return (
      // <View>
      <TextField
        style={Styles.textInputStyle}
        ref={ref => (this.textField = ref)}
        returnKeyType={returnKeyType}
        keyboardType={keyboardType}
        error={hasError ? errorText : ""}
        errorColor={hasError ? "red" : "#000000"}
        label={label}
        value={textValue}
        secureTextEntry={secureTextEntry}
        onSubmitEditing={this.handleTextSubmit.bind(this)}
        onChangeText={this.handleTextInput.bind(this)}
      />
      //   </View>
    );
  }
}
